var console = require('./debug.js')
var statusStore = require('./mqttStatus.js').statusStore
var settingStore = require('./mqttSettings.js').settingStore
var functionStore = require('./mqttFunctions.js').functionStore

class Axis {
	constructor(options) {
		console.group("new Axis");
		console.log("internalName: " + options.internalName)
		console.log("axisName: " + options.axisName)
		console.log("axisLetter: " + options.axisLetter)

		this.internalName = options.internalName //for actual use in interface implemetation e.g. grbl axis name
		this.axisName = options.axisName //for mqtt topic
		this.axisLetter = options.axisLetter //for toString method
		this.interface = options.interface //reference of the containing interface
		this.jogMin = (options.jogMin != undefined ? options.jogMin : 0.05) //minimum absolute value of jogfactor. creates deadzone in the center

		this.basetopic = this.interface.basetopic + "/" + this.axisName
		this.moveTo = functionStore.addFunction({
			name: this.basetopic + "/moveTo",
			callback: (target) => {
				console.log(this.internalName + " move to " + target);
				console.log(target);
				var buf = Buffer.from(target)
				console.log(buf.toString());
				options.moveTo({
					internalName: this.internalName,
					axisName: this.axisName,
					axisLetter: this.axisLetter,
					include: this.isIncluded(),
					value: buf.toString()
				})
			},
			topic: this.basetopic + "/moveTo"
		})

		this.value = statusStore.addStatus({
			name: this.basetopic + "/value",
			value: 0,
			topic: this.basetopic + "/value"
		})

		this.include = settingStore.addSetting({
			name: this.basetopic + "/include",
			value: true,
			callback: undefined,
			topic: this.basetopic + "/include"
		})
		this.jogfactor = settingStore.addSetting({
			name: this.basetopic + "/jogfactor",
			value: 0,
			callback: undefined,
			topic: this.basetopic + "/jogfactor"
		})
		this.toggleInclude = functionStore.addFunction({
			name: this.basetopic + "/toggleInclude",
			callback: () => {
				this.include.value = !this.include.value;
			},
			topic: this.basetopic + "/toggleInclude",
		})
		console.groupEnd();
	}
	toString() {
		return ("  " + this.getValue().toFixed(2)).slice(-6) + this.axisLetter + (this.isIncluded() ? "<" : " ")
	}
	getValue() {
		return this.value.value
	}
	setValue(value) {
		this.value.update(value)
	}
	isIncluded() {
		return this.include.value
	}
	getJogFactor() {
		return this.jogfactor.value
	}
	setJogFactor(value) {
		return this.jogfactor.update(value)
	}
	getData() {
		return {
			internalName: this.internalName,
			axisName: this.axisName,
			axisLetter: this.axisLetter,
			include: this.isIncluded(),
			value: this.getValue()
		}
	}

}

module.exports = Axis
