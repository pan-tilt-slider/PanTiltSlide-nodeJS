var mqtt = require("./mqttHandler.js").mqttHandler
var console = require('./debug.js')

const CONTROLLER_BASETOPIC = require("./mqttBasetopics.js").CONTROLLER_BASETOPIC

// software counterpart for my physical mqtt remote controller

const buttonNames = [
    "L1",
    "L2",
    "L3",
    "UP",
    "DOWN",
    "LEFT",
    "RIGHT",
    "SELECT",
    "HOME",
    "START",
    "R1",
    "R2",
    "R3",
    "X",
    "Y",
    "A",
    "B",
    "PLUS",
    "MINUS",
    "NEXT",
    "PREV",
    "PLAY"
]

const axisNames = [
    "RX",
    "RY",
    "LX",
    "LY",
    "R",
    "L"
]

class controller {
    constructor() {
        console.group("setup controller")
        /**
         * holds all button objects
         * @type {Object}
         */
        this.buttons = {}

        this.birthCallback = () => {}

        //assign empty callbacks
        buttonNames.forEach((name) => {
            // console.log(name);
            this.buttons[name] = {};
            this.buttons[name].name = name;
            this.buttons[name].value = () => {};
            this.buttons[name].rise = () => {};
            this.buttons[name].fall = () => {};

            mqtt.subscribe(CONTROLLER_BASETOPIC + "/" + name + "/R/value", (value) => {
                this.buttons[name].value()
            })
            mqtt.subscribe(CONTROLLER_BASETOPIC + "/" + name + "/R/rise", (value) => {
                this.buttons[name].rise()
            })
            mqtt.subscribe(CONTROLLER_BASETOPIC + "/" + name + "/R/fall", (value) => {
                this.buttons[name].fall()
            })
        });

        mqtt.subscribe(CONTROLLER_BASETOPIC + "/birth/R/", (value) => {
            this.birthCallback(value)
        })


        //the callbacks for the joystic axis
        this.axis = {};

        axisNames.forEach((name) => {
            this.axis[name] = {}
            this.axis[name].name = name
            this.axis[name].event = () => {}
            this.axis[name].value = () => {}
            this.axis[name].center = () => {}

            mqtt.subscribe(CONTROLLER_BASETOPIC + "/" + name + "/R/value", (value) => {
                value = value.toString()
                this.axisPosition[name] = value
                this.axis[name].value(value)
            })
            mqtt.subscribe(CONTROLLER_BASETOPIC + "/" + name + "/R/event", (value) => {
                value = value.toString()
                this.axisPosition[name] = value
                this.axis[name].event(value)
            })
            mqtt.subscribe(CONTROLLER_BASETOPIC + "/" + name + "/R/center", (value) => {
                value = value.toString()
                this.axisPosition[name] = value
                this.axis[name].center(value)
            })
            // TODO: axis center callback
        });

        this.axisPosition = {
            RX: 0,
            RY: 0,
            LX: 0,
            LY: 0
        }
        console.groupEnd()
    }



    /**
     * assign callbacks to the controller buttons
     * @param  {object} callbacks {name: callback, name2: callback2, ...}
     */
    assignButtonCallbacks(callbacks) {
        callbacks.forEach((callback) => {
            try {
                this.buttons[callback.name][callback.trigger] = callback.callback
            } catch (e) {
                console.error(e);
                console.error("BUTTON NOT DEFINED");
                console.error(callback);
            }
        });

    }

    /**
     * assign callbacks to the controller joystic axis
     * @param  {object} callbacks {name: callback, name2: callback2, ...}
     */
    assignAxisCallbacks(callbacks) {
        callbacks.forEach((callback) => {
            try {
                this.axis[callback.name][callback.trigger] = callback.callback
            } catch (e) {
                console.error(e);
                console.error("AXIS NOT DEFINED");
                console.error(callback);
            }
        });
    }
}
// only one controller instance allowed
var CONTROLLER = new controller();

module.exports.controller = CONTROLLER;
