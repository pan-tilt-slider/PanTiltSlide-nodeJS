var controller = new require("./controller.js").controller;
var mqtt = require("./mqttHandler.js").mqttHandler;
var Gui = require("./gui.js");
var gui = new Gui({
	name: "gui"
})
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const port = new SerialPort('/dev/serial0', {
	baudRate: 115200
})
const parser = new Readline()
port.pipe(parser)
// read from serial
parser.on('data', line => {
	console.log("<< " + line)
});

var stdinReadline = require('readline');
var rl = stdinReadline.createInterface({
	input: process.stdin,
	output: process.stdout,
	terminal: false
});

rl.on('line', (line) => {
	console.log(">>" + line);
	port.write(Buffer.from(line + '\r', 'utf-8'), err => {
		if (err) console.log(err);
	});
})
