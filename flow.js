[
    {
        "id": "7820a8e.84a8458",
        "type": "tab",
        "label": "Settings",
        "disabled": false,
        "info": ""
    },
    {
        "id": "25436c92.733984",
        "type": "tab",
        "label": "Status",
        "disabled": false,
        "info": ""
    },
    {
        "id": "440cacce.57aac4",
        "type": "tab",
        "label": "Functions",
        "disabled": false,
        "info": ""
    },
    {
        "id": "6684ba10.301104",
        "type": "tab",
        "label": "Keyframe Editor",
        "disabled": false,
        "info": ""
    },
    {
        "id": "07168438cc3bbe1a",
        "type": "tab",
        "label": "Control",
        "disabled": false,
        "info": "",
        "env": []
    },
    {
        "id": "5b9ee607.66d238",
        "type": "ui_base",
        "theme": {
            "name": "theme-dark",
            "lightTheme": {
                "default": "#0094CE",
                "baseColor": "#0094CE",
                "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
                "edited": true,
                "reset": false
            },
            "darkTheme": {
                "default": "#097479",
                "baseColor": "#097479",
                "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
                "edited": true,
                "reset": false
            },
            "customTheme": {
                "name": "Untitled Theme 1",
                "default": "#4B7930",
                "baseColor": "#783030",
                "baseFont": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif",
                "reset": false
            },
            "themeState": {
                "base-color": {
                    "default": "#097479",
                    "value": "#097479",
                    "edited": true
                },
                "page-titlebar-backgroundColor": {
                    "value": "#097479",
                    "edited": false
                },
                "page-backgroundColor": {
                    "value": "#111111",
                    "edited": false
                },
                "page-sidebar-backgroundColor": {
                    "value": "#333333",
                    "edited": false
                },
                "group-textColor": {
                    "value": "#0eb8c0",
                    "edited": false
                },
                "group-borderColor": {
                    "value": "#555555",
                    "edited": false
                },
                "group-backgroundColor": {
                    "value": "#333333",
                    "edited": false
                },
                "widget-textColor": {
                    "value": "#eeeeee",
                    "edited": false
                },
                "widget-backgroundColor": {
                    "value": "#097479",
                    "edited": false
                },
                "widget-borderColor": {
                    "value": "#333333",
                    "edited": false
                },
                "base-font": {
                    "value": "-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif"
                }
            },
            "angularTheme": {
                "primary": "indigo",
                "accents": "blue",
                "warn": "red",
                "background": "grey",
                "palette": "light"
            }
        },
        "site": {
            "name": "Node-RED Dashboard",
            "hideToolbar": "true",
            "allowSwipe": "false",
            "lockMenu": "icon",
            "allowTempTheme": "true",
            "dateFormat": "DD/MM/YYYY",
            "sizes": {
                "sx": 33,
                "sy": 33,
                "gx": 6,
                "gy": 6,
                "cx": 6,
                "cy": 6,
                "px": 0,
                "py": 0
            }
        }
    },
    {
        "id": "3236656.d10759a",
        "type": "ui_tab",
        "name": "Pan Tilt Slider",
        "icon": "laptop",
        "order": 2,
        "disabled": false,
        "hidden": false
    },
    {
        "id": "b2bea16b.4dd8d",
        "type": "mqtt-broker",
        "name": "",
        "broker": "localhost",
        "port": "1883",
        "clientid": "",
        "autoConnect": true,
        "usetls": false,
        "compatmode": false,
        "protocolVersion": "4",
        "keepalive": "60",
        "cleansession": true,
        "birthTopic": "",
        "birthQos": "0",
        "birthPayload": "",
        "birthMsg": {},
        "closeTopic": "",
        "closeQos": "0",
        "closePayload": "",
        "closeMsg": {},
        "willTopic": "",
        "willQos": "0",
        "willPayload": "",
        "willMsg": {},
        "userProps": "",
        "sessionExpiry": ""
    },
    {
        "id": "ad53d039.25fc",
        "type": "ui_group",
        "name": "Status",
        "tab": "3236656.d10759a",
        "order": 3,
        "disp": false,
        "width": "7",
        "collapse": false
    },
    {
        "id": "d4a6b79b.2299a8",
        "type": "ui_group",
        "name": "grbl control",
        "tab": "3236656.d10759a",
        "order": 1,
        "disp": true,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "3f3cf4be.ea33ac",
        "type": "ui_group",
        "name": "Settings",
        "tab": "3236656.d10759a",
        "order": 4,
        "disp": false,
        "width": 2,
        "collapse": false
    },
    {
        "id": "201c8997.ed6256",
        "type": "ui_tab",
        "name": "Keyframe Editor",
        "icon": "code",
        "order": 1,
        "disabled": false,
        "hidden": false
    },
    {
        "id": "6c9bc8e8.0ad8d8",
        "type": "ui_group",
        "name": "Coordinates",
        "tab": "3236656.d10759a",
        "order": 2,
        "disp": true,
        "width": "7",
        "collapse": false,
        "className": ""
    },
    {
        "id": "d8df1128.d80cf",
        "type": "ui_group",
        "name": "Include",
        "tab": "08012ada27372baf",
        "order": 3,
        "disp": false,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "5803007b2a0dcf5f",
        "type": "ui_group",
        "name": "Keyframes",
        "tab": "201c8997.ed6256",
        "order": 1,
        "disp": true,
        "width": "18",
        "collapse": false,
        "className": ""
    },
    {
        "id": "1631f553c2fec44c",
        "type": "ui_group",
        "name": "KF Control",
        "tab": "08012ada27372baf",
        "order": 7,
        "disp": false,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "607f818ccaddea8f",
        "type": "ui_group",
        "name": "Status",
        "tab": "08012ada27372baf",
        "order": 2,
        "disp": false,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "b605a216fbcede99",
        "type": "ui_group",
        "name": "Time",
        "tab": "08012ada27372baf",
        "order": 5,
        "disp": false,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "08012ada27372baf",
        "type": "ui_tab",
        "name": "Control",
        "icon": "games",
        "disabled": false,
        "hidden": false
    },
    {
        "id": "5654c0f3536c995a",
        "type": "ui_group",
        "name": "Control Left",
        "tab": "08012ada27372baf",
        "order": 4,
        "disp": false,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "38ff8262509bdeed",
        "type": "ui_group",
        "name": "gbl",
        "tab": "08012ada27372baf",
        "order": 1,
        "disp": false,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "98a790d19ee0eaef",
        "type": "ui_group",
        "name": "Control Right",
        "tab": "08012ada27372baf",
        "order": 6,
        "disp": false,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "c23deb21fe5d2555",
        "type": "ui_group",
        "name": "Max jog speed",
        "tab": "08012ada27372baf",
        "order": 8,
        "disp": false,
        "width": "6",
        "collapse": false,
        "className": ""
    },
    {
        "id": "82f129ad.1e98a8",
        "type": "mqtt in",
        "z": "25436c92.733984",
        "name": "",
        "topic": "interface/panTiltSlider/status/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "inputs": 0,
        "x": 390,
        "y": 100,
        "wires": [
            [
                "4b62865e.e57f38",
                "84e46d73.85bed"
            ]
        ]
    },
    {
        "id": "4b62865e.e57f38",
        "type": "ui_text",
        "z": "25436c92.733984",
        "group": "ad53d039.25fc",
        "order": 1,
        "width": 5,
        "height": 1,
        "name": "",
        "label": "Status",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 610,
        "y": 100,
        "wires": []
    },
    {
        "id": "b40e586.9b1c6a8",
        "type": "mqtt in",
        "z": "25436c92.733984",
        "name": "",
        "topic": "interface/panTiltSlider/pan/value/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 380,
        "y": 300,
        "wires": [
            [
                "d47f3328.e6629"
            ]
        ]
    },
    {
        "id": "d47f3328.e6629",
        "type": "ui_text",
        "z": "25436c92.733984",
        "group": "6c9bc8e8.0ad8d8",
        "order": 3,
        "width": 3,
        "height": 1,
        "name": "",
        "label": "pan",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "className": "",
        "x": 610,
        "y": 300,
        "wires": []
    },
    {
        "id": "b3ce40da.30257",
        "type": "mqtt in",
        "z": "25436c92.733984",
        "name": "",
        "topic": "interface/panTiltSlider/tilt/value/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 370,
        "y": 360,
        "wires": [
            [
                "568ac063.2076"
            ]
        ]
    },
    {
        "id": "568ac063.2076",
        "type": "ui_text",
        "z": "25436c92.733984",
        "group": "6c9bc8e8.0ad8d8",
        "order": 5,
        "width": 3,
        "height": 1,
        "name": "",
        "label": "tilt",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "className": "",
        "x": 610,
        "y": 360,
        "wires": []
    },
    {
        "id": "b00c50aa.e138f",
        "type": "mqtt in",
        "z": "25436c92.733984",
        "name": "",
        "topic": "interface/panTiltSlider/slide/value/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 380,
        "y": 420,
        "wires": [
            [
                "ada42296.97de4"
            ]
        ]
    },
    {
        "id": "ada42296.97de4",
        "type": "ui_text",
        "z": "25436c92.733984",
        "group": "6c9bc8e8.0ad8d8",
        "order": 1,
        "width": 3,
        "height": 1,
        "name": "",
        "label": "slide",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "className": "",
        "x": 610,
        "y": 420,
        "wires": []
    },
    {
        "id": "eeffabe6.c0ae98",
        "type": "mqtt in",
        "z": "25436c92.733984",
        "name": "",
        "topic": "interface/panTiltSlider/error/message/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "inputs": 0,
        "x": 360,
        "y": 220,
        "wires": [
            [
                "5439b22f.6ffbdc",
                "b7c9ef84.7d035"
            ]
        ]
    },
    {
        "id": "5439b22f.6ffbdc",
        "type": "ui_text",
        "z": "25436c92.733984",
        "group": "ad53d039.25fc",
        "order": 5,
        "width": 5,
        "height": 1,
        "name": "",
        "label": "Error",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 610,
        "y": 220,
        "wires": []
    },
    {
        "id": "f9481087.23606",
        "type": "mqtt in",
        "z": "25436c92.733984",
        "name": "",
        "topic": "interface/panTiltSlider/alarm/message/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "inputs": 0,
        "x": 360,
        "y": 160,
        "wires": [
            [
                "2426f313.89e77c",
                "1893bfd2.ef2c9"
            ]
        ]
    },
    {
        "id": "2426f313.89e77c",
        "type": "ui_text",
        "z": "25436c92.733984",
        "group": "ad53d039.25fc",
        "order": 3,
        "width": 5,
        "height": 1,
        "name": "",
        "label": "Alarm",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "x": 610,
        "y": 160,
        "wires": []
    },
    {
        "id": "4a450980.7853e8",
        "type": "ui_level",
        "z": "25436c92.733984",
        "group": "6c9bc8e8.0ad8d8",
        "order": 4,
        "width": "3",
        "height": "1",
        "name": "pan",
        "label": "pan",
        "colorHi": "#e60000",
        "colorWarn": "#ff9900",
        "colorNormal": "#00b33c",
        "colorOff": "#595959",
        "min": "-1",
        "max": "1",
        "segWarn": "",
        "segHigh": "",
        "unit": "",
        "layout": "sh",
        "channelA": "",
        "channelB": "",
        "decimals": "2",
        "animations": "reactive",
        "shape": "2",
        "colorschema": "fixed",
        "textoptions": "default",
        "colorText": "#eeeeee",
        "fontLabel": "",
        "fontValue": "",
        "fontSmall": "",
        "colorFromTheme": true,
        "textAnimations": true,
        "hideValue": false,
        "tickmode": "off",
        "peakmode": false,
        "property": "payload",
        "peaktime": 3000,
        "x": 610,
        "y": 580,
        "wires": []
    },
    {
        "id": "e3d56ebd.a6d04",
        "type": "ui_level",
        "z": "25436c92.733984",
        "group": "6c9bc8e8.0ad8d8",
        "order": 2,
        "width": "3",
        "height": "1",
        "name": "slide",
        "label": "slide",
        "colorHi": "#e60000",
        "colorWarn": "#ff9900",
        "colorNormal": "#00b33c",
        "colorOff": "#595959",
        "min": "-1",
        "max": "1",
        "segWarn": "",
        "segHigh": "",
        "unit": "",
        "layout": "sh",
        "channelA": "",
        "channelB": "",
        "decimals": "2",
        "animations": "reactive",
        "shape": "2",
        "colorschema": "fixed",
        "textoptions": "default",
        "colorText": "#eeeeee",
        "fontLabel": "",
        "fontValue": "",
        "fontSmall": "",
        "colorFromTheme": true,
        "textAnimations": true,
        "hideValue": false,
        "tickmode": "off",
        "peakmode": false,
        "property": "payload",
        "peaktime": 3000,
        "x": 610,
        "y": 700,
        "wires": []
    },
    {
        "id": "deb44cee.6a4b6",
        "type": "ui_level",
        "z": "25436c92.733984",
        "group": "6c9bc8e8.0ad8d8",
        "order": 6,
        "width": "3",
        "height": "1",
        "name": "tilt",
        "label": "tilt",
        "colorHi": "#e60000",
        "colorWarn": "#ff9900",
        "colorNormal": "#00b33c",
        "colorOff": "#595959",
        "min": "-1",
        "max": "1",
        "segWarn": "",
        "segHigh": "",
        "unit": "",
        "layout": "sh",
        "channelA": "",
        "channelB": "",
        "decimals": "2",
        "animations": "reactive",
        "shape": 2,
        "colorschema": "fixed",
        "textoptions": "default",
        "colorText": "#eeeeee",
        "fontLabel": "",
        "fontValue": "",
        "fontSmall": "",
        "colorFromTheme": true,
        "textAnimations": true,
        "hideValue": false,
        "tickmode": "off",
        "peakmode": false,
        "property": "payload",
        "peaktime": 3000,
        "x": 610,
        "y": 640,
        "wires": []
    },
    {
        "id": "84e46d73.85bed",
        "type": "ui_led",
        "z": "25436c92.733984",
        "order": 2,
        "group": "ad53d039.25fc",
        "width": 2,
        "height": 1,
        "label": "",
        "labelPlacement": "left",
        "labelAlignment": "left",
        "colorForValue": [
            {
                "color": "green",
                "value": "Idle",
                "valueType": "str"
            },
            {
                "color": "yellow",
                "value": "Jog",
                "valueType": "str"
            },
            {
                "color": "blue",
                "value": "Run",
                "valueType": "str"
            },
            {
                "color": "red",
                "value": "Alarm",
                "valueType": "str"
            }
        ],
        "allowColorForValueInMessage": false,
        "name": "Status",
        "x": 890,
        "y": 100,
        "wires": []
    },
    {
        "id": "75744a5e.d31944",
        "type": "ui_led",
        "z": "25436c92.733984",
        "order": 4,
        "group": "ad53d039.25fc",
        "width": 2,
        "height": 1,
        "label": "",
        "labelPlacement": "left",
        "labelAlignment": "left",
        "colorForValue": [
            {
                "color": "green",
                "value": "true",
                "valueType": "bool"
            },
            {
                "color": "red",
                "value": "false",
                "valueType": "bool"
            }
        ],
        "allowColorForValueInMessage": false,
        "name": "Alarm",
        "x": 890,
        "y": 160,
        "wires": []
    },
    {
        "id": "1893bfd2.ef2c9",
        "type": "function",
        "z": "25436c92.733984",
        "name": "",
        "func": "msg.payload = msg.payload == \"\"\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 740,
        "y": 160,
        "wires": [
            [
                "75744a5e.d31944"
            ]
        ]
    },
    {
        "id": "6d400c01.e25ee4",
        "type": "ui_led",
        "z": "25436c92.733984",
        "order": 6,
        "group": "ad53d039.25fc",
        "width": 2,
        "height": 1,
        "label": "",
        "labelPlacement": "left",
        "labelAlignment": "left",
        "colorForValue": [
            {
                "color": "green",
                "value": "true",
                "valueType": "bool"
            },
            {
                "color": "red",
                "value": "false",
                "valueType": "bool"
            }
        ],
        "allowColorForValueInMessage": false,
        "name": "Error",
        "x": 890,
        "y": 220,
        "wires": []
    },
    {
        "id": "b7c9ef84.7d035",
        "type": "function",
        "z": "25436c92.733984",
        "name": "",
        "func": "msg.payload = msg.payload == \"\"\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 750,
        "y": 220,
        "wires": [
            [
                "6d400c01.e25ee4"
            ]
        ]
    },
    {
        "id": "c1196d134784e4cc",
        "type": "mqtt in",
        "z": "25436c92.733984",
        "name": "",
        "topic": "interface/panTiltSlider/pan/jogfactor/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 310,
        "y": 580,
        "wires": [
            [
                "4a450980.7853e8"
            ]
        ]
    },
    {
        "id": "dc29edf0dc3f1cd5",
        "type": "mqtt in",
        "z": "25436c92.733984",
        "name": "",
        "topic": "interface/panTiltSlider/tilt/jogfactor/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 300,
        "y": 640,
        "wires": [
            [
                "deb44cee.6a4b6"
            ]
        ]
    },
    {
        "id": "f16052683f270e71",
        "type": "mqtt in",
        "z": "25436c92.733984",
        "name": "",
        "topic": "interface/panTiltSlider/slide/jogfactor/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 310,
        "y": 700,
        "wires": [
            [
                "e3d56ebd.a6d04"
            ]
        ]
    },
    {
        "id": "84498ab2.c9c5a8",
        "type": "mqtt out",
        "z": "440cacce.57aac4",
        "name": "",
        "topic": "interface/panTiltSlider/killAlarmLock/W",
        "qos": "",
        "retain": "",
        "broker": "b2bea16b.4dd8d",
        "x": 650,
        "y": 140,
        "wires": []
    },
    {
        "id": "4e9935de.9d9f9c",
        "type": "ui_button",
        "z": "440cacce.57aac4",
        "name": "",
        "group": "d4a6b79b.2299a8",
        "order": 1,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Kill Alarm",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "topicType": "str",
        "x": 360,
        "y": 140,
        "wires": [
            [
                "84498ab2.c9c5a8"
            ]
        ]
    },
    {
        "id": "e9082247.c8e88",
        "type": "mqtt out",
        "z": "440cacce.57aac4",
        "name": "",
        "topic": "interface/panTiltSlider/softReset/W",
        "qos": "",
        "retain": "",
        "broker": "b2bea16b.4dd8d",
        "x": 640,
        "y": 200,
        "wires": []
    },
    {
        "id": "bc233e37.a8d17",
        "type": "ui_button",
        "z": "440cacce.57aac4",
        "name": "",
        "group": "d4a6b79b.2299a8",
        "order": 2,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Soft Reset",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "x": 350,
        "y": 200,
        "wires": [
            [
                "e9082247.c8e88"
            ]
        ]
    },
    {
        "id": "9dab2191.62839",
        "type": "mqtt out",
        "z": "440cacce.57aac4",
        "name": "",
        "topic": "interface/panTiltSlider/resume/W",
        "qos": "",
        "retain": "",
        "broker": "b2bea16b.4dd8d",
        "x": 630,
        "y": 260,
        "wires": []
    },
    {
        "id": "b9651732.20cbc8",
        "type": "ui_button",
        "z": "440cacce.57aac4",
        "name": "",
        "group": "d4a6b79b.2299a8",
        "order": 3,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Resume",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "x": 360,
        "y": 260,
        "wires": [
            [
                "9dab2191.62839"
            ]
        ]
    },
    {
        "id": "86f47344.edbae",
        "type": "mqtt out",
        "z": "440cacce.57aac4",
        "name": "",
        "topic": "keyframeEditor/playAllKeyframes/W",
        "qos": "",
        "retain": "",
        "broker": "b2bea16b.4dd8d",
        "x": 700,
        "y": 320,
        "wires": []
    },
    {
        "id": "bcd39d06.c8f99",
        "type": "ui_button",
        "z": "440cacce.57aac4",
        "name": "",
        "group": "d4a6b79b.2299a8",
        "order": 4,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Play Keyframes",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "x": 340,
        "y": 320,
        "wires": [
            [
                "86f47344.edbae"
            ]
        ]
    },
    {
        "id": "931ee67.2308418",
        "type": "mqtt out",
        "z": "440cacce.57aac4",
        "name": "",
        "topic": "interface/panTiltSlider/feedHold/W",
        "qos": "",
        "retain": "",
        "broker": "b2bea16b.4dd8d",
        "x": 630,
        "y": 380,
        "wires": []
    },
    {
        "id": "254d0b22.38f854",
        "type": "ui_button",
        "z": "440cacce.57aac4",
        "name": "",
        "group": "d4a6b79b.2299a8",
        "order": 5,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Feed Hold",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "x": 350,
        "y": 380,
        "wires": [
            [
                "931ee67.2308418"
            ]
        ]
    },
    {
        "id": "f80a71a8.f4fb4",
        "type": "mqtt out",
        "z": "440cacce.57aac4",
        "name": "",
        "topic": "interface/panTiltSlider/jogCancel/W",
        "qos": "",
        "retain": "",
        "broker": "b2bea16b.4dd8d",
        "x": 640,
        "y": 440,
        "wires": []
    },
    {
        "id": "7fa3bf46.49b2e",
        "type": "ui_button",
        "z": "440cacce.57aac4",
        "name": "",
        "group": "d4a6b79b.2299a8",
        "order": 6,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Jog Cancel",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "x": 350,
        "y": 440,
        "wires": [
            [
                "f80a71a8.f4fb4"
            ]
        ]
    },
    {
        "id": "f2efe9e5af65d5a7",
        "type": "mqtt out",
        "z": "440cacce.57aac4",
        "name": "",
        "topic": "interface/panTiltSlider/home/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 630,
        "y": 520,
        "wires": []
    },
    {
        "id": "d9ad3906bfb4312f",
        "type": "ui_button",
        "z": "440cacce.57aac4",
        "name": "",
        "group": "d4a6b79b.2299a8",
        "order": 6,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Homing",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "topicType": "str",
        "x": 360,
        "y": 520,
        "wires": [
            [
                "f2efe9e5af65d5a7"
            ]
        ]
    },
    {
        "id": "518aee0675827c77",
        "type": "ui_table",
        "z": "6684ba10.301104",
        "group": "5803007b2a0dcf5f",
        "name": "",
        "order": 6,
        "width": "18",
        "height": "10",
        "columns": [],
        "outputs": 0,
        "cts": false,
        "x": 730,
        "y": 240,
        "wires": []
    },
    {
        "id": "50dc6f59a94ca51c",
        "type": "mqtt in",
        "z": "6684ba10.301104",
        "name": "",
        "topic": "keyframeEditor/keyframes/R",
        "qos": "2",
        "datatype": "auto-detect",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": true,
        "rh": 0,
        "inputs": 0,
        "x": 360,
        "y": 240,
        "wires": [
            [
                "242e7db94e3f0f81"
            ]
        ]
    },
    {
        "id": "242e7db94e3f0f81",
        "type": "function",
        "z": "6684ba10.301104",
        "name": "function 1",
        "func": "var tableOfKeyframes = []\nvar index = 0;\n\nfunction convertTime(timeValue){\n    var time = {\n        tenthSeconds: timeValue % 10,\n        seconds: Math.floor(timeValue / 10) % 60,\n        minutes: Math.floor(timeValue / 600),\n    }\n    return (\"0\" + time.minutes).slice(-2) + \":\" + (\"0\" + time.seconds).slice(-2) + \".\" + time.tenthSeconds\n}\n\nmsg.payload.forEach(function(keyframe) {\n    var entry = {}\n    entry[\"Index\"] = index\n    entry[\"Time\"] = convertTime(keyframe.t)\n    Object.keys(keyframe.interfaces).forEach((name)=>{\n        let interface = keyframe.interfaces[name]\n        // node.warn(interface.name);\n        \n        Object.keys(interface.axes).forEach((name) => {\n            let axis = interface.axes[name]\n\n            // node.warn(axis.axisName);\n            entry[axis.axisLetter + \n                \" value\"] = axis.value\n            entry[axis.axisLetter +\n                \" include\"] = axis.include\n        })\n    })\n    tableOfKeyframes.push(entry)\n    index++;\n});\nmsg.payload = tableOfKeyframes\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "initialize": "",
        "finalize": "",
        "libs": [],
        "x": 580,
        "y": 240,
        "wires": [
            [
                "518aee0675827c77"
            ]
        ]
    },
    {
        "id": "8a429a3c82adf456",
        "type": "ui_joystick",
        "z": "07168438cc3bbe1a",
        "name": "Left Stick",
        "group": "5654c0f3536c995a",
        "order": 1,
        "width": "6",
        "height": "5",
        "trigger": "all",
        "timeInterval": "200",
        "useThemeColor": true,
        "color": "#000000",
        "threshold": 0.1,
        "directions": "all",
        "shape": "circle",
        "centerAtRelease": true,
        "x": 320,
        "y": 300,
        "wires": [
            [
                "9a23930750e79f9f",
                "170449b4a0e48f11"
            ]
        ]
    },
    {
        "id": "9a23930750e79f9f",
        "type": "debug",
        "z": "07168438cc3bbe1a",
        "name": "debug 1",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "payload.vector.y",
        "targetType": "msg",
        "statusVal": "",
        "statusType": "auto",
        "x": 610,
        "y": 360,
        "wires": []
    },
    {
        "id": "170449b4a0e48f11",
        "type": "debug",
        "z": "07168438cc3bbe1a",
        "name": "debug 2",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "payload.vector.x",
        "targetType": "msg",
        "statusVal": "",
        "statusType": "auto",
        "x": 610,
        "y": 300,
        "wires": []
    },
    {
        "id": "57327786.108778",
        "type": "mqtt in",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/pan/include/R",
        "qos": "0",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 240,
        "y": 500,
        "wires": [
            [
                "bb88c229.6ac35"
            ]
        ]
    },
    {
        "id": "f829c412.5d6cb8",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/pan/include/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 710,
        "y": 500,
        "wires": []
    },
    {
        "id": "bb88c229.6ac35",
        "type": "ui_switch",
        "z": "07168438cc3bbe1a",
        "name": "",
        "label": "pan",
        "tooltip": "",
        "group": "d8df1128.d80cf",
        "order": 0,
        "width": "6",
        "height": "1",
        "passthru": false,
        "decouple": "true",
        "topic": "",
        "topicType": "str",
        "style": "",
        "onvalue": "true",
        "onvalueType": "str",
        "onicon": "",
        "oncolor": "",
        "offvalue": "false",
        "offvalueType": "str",
        "officon": "",
        "offcolor": "",
        "animate": true,
        "className": "",
        "x": 470,
        "y": 500,
        "wires": [
            [
                "f829c412.5d6cb8"
            ]
        ]
    },
    {
        "id": "86f1a38b.1a16f",
        "type": "mqtt in",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/tilt/include/R",
        "qos": "0",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 240,
        "y": 560,
        "wires": [
            [
                "b8c87cd.afdba8"
            ]
        ]
    },
    {
        "id": "6b20e6ad.481238",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/tilt/include/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 700,
        "y": 560,
        "wires": []
    },
    {
        "id": "b8c87cd.afdba8",
        "type": "ui_switch",
        "z": "07168438cc3bbe1a",
        "name": "",
        "label": "tilt",
        "tooltip": "",
        "group": "d8df1128.d80cf",
        "order": 0,
        "width": "6",
        "height": "1",
        "passthru": false,
        "decouple": "true",
        "topic": "",
        "topicType": "str",
        "style": "",
        "onvalue": "true",
        "onvalueType": "str",
        "onicon": "",
        "oncolor": "",
        "offvalue": "false",
        "offvalueType": "str",
        "officon": "",
        "offcolor": "",
        "animate": true,
        "className": "",
        "x": 470,
        "y": 560,
        "wires": [
            [
                "6b20e6ad.481238"
            ]
        ]
    },
    {
        "id": "db1da177.88c2e",
        "type": "mqtt in",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/slide/include/R",
        "qos": "0",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 240,
        "y": 620,
        "wires": [
            [
                "b898f6b8.4bbba8"
            ]
        ]
    },
    {
        "id": "3f870fde.ecfb2",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/slide/include/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 710,
        "y": 620,
        "wires": []
    },
    {
        "id": "b898f6b8.4bbba8",
        "type": "ui_switch",
        "z": "07168438cc3bbe1a",
        "name": "",
        "label": "slide",
        "tooltip": "",
        "group": "d8df1128.d80cf",
        "order": 0,
        "width": "6",
        "height": "1",
        "passthru": false,
        "decouple": "true",
        "topic": "",
        "topicType": "str",
        "style": "",
        "onvalue": "true",
        "onvalueType": "str",
        "onicon": "",
        "oncolor": "",
        "offvalue": "false",
        "offvalueType": "str",
        "officon": "",
        "offcolor": "",
        "animate": true,
        "className": "",
        "x": 470,
        "y": 620,
        "wires": [
            [
                "3f870fde.ecfb2"
            ]
        ]
    },
    {
        "id": "d7d7bf395cd5d218",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "Add one keyframe",
        "group": "1631f553c2fec44c",
        "order": 1,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "Add one keyframe",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "add",
        "payload": "",
        "payloadType": "date",
        "topic": "topic",
        "topicType": "msg",
        "x": 350,
        "y": 720,
        "wires": [
            [
                "fbb1570e80127c07"
            ]
        ]
    },
    {
        "id": "fbb1570e80127c07",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/addKeyframe/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 610,
        "y": 720,
        "wires": []
    },
    {
        "id": "e03c34b3342c78cf",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "Delete",
        "group": "1631f553c2fec44c",
        "order": 2,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "Delete one keyframe",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "remove",
        "payload": "",
        "payloadType": "date",
        "topic": "topic",
        "topicType": "msg",
        "x": 390,
        "y": 780,
        "wires": [
            [
                "2df764a954e66b88"
            ]
        ]
    },
    {
        "id": "f3d452e5d5e2c4f8",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "Clear all",
        "group": "1631f553c2fec44c",
        "order": 3,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "delete",
        "payload": "",
        "payloadType": "date",
        "topic": "topic",
        "topicType": "msg",
        "x": 380,
        "y": 840,
        "wires": [
            [
                "6b38e9debc91a67c"
            ]
        ]
    },
    {
        "id": "2df764a954e66b88",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/deleteKeyframe/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 620,
        "y": 780,
        "wires": []
    },
    {
        "id": "6b38e9debc91a67c",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/clearAllKeyframes/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 630,
        "y": 840,
        "wires": []
    },
    {
        "id": "876a09be9d4515e1",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "Inc.",
        "group": "1631f553c2fec44c",
        "order": 9,
        "width": "1",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "chevron_right",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 390,
        "y": 1080,
        "wires": [
            [
                "ab8d9664b1c71536"
            ]
        ]
    },
    {
        "id": "de1f99021b8ea32b",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "Dec.",
        "group": "1631f553c2fec44c",
        "order": 7,
        "width": "1",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "chevron_left",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 390,
        "y": 960,
        "wires": [
            [
                "494c3172ef90cdbe"
            ]
        ]
    },
    {
        "id": "ab8d9664b1c71536",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/incKeyframePointer/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 630,
        "y": 1080,
        "wires": []
    },
    {
        "id": "494c3172ef90cdbe",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/decKeyframePointer/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 630,
        "y": 960,
        "wires": []
    },
    {
        "id": "05e8b547503632e9",
        "type": "ui_text",
        "z": "07168438cc3bbe1a",
        "group": "607f818ccaddea8f",
        "order": 3,
        "width": 0,
        "height": 0,
        "name": "",
        "label": "KF count",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "className": "",
        "x": 1260,
        "y": 780,
        "wires": []
    },
    {
        "id": "390f92f659f4c99b",
        "type": "mqtt in",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/keyframePointer/R",
        "qos": "2",
        "datatype": "auto-detect",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": true,
        "rh": 0,
        "inputs": 0,
        "x": 1020,
        "y": 720,
        "wires": [
            [
                "9d10dd42462bea2f"
            ]
        ]
    },
    {
        "id": "d942f3e0f9412f7b",
        "type": "mqtt in",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/keyframeCount/R",
        "qos": "2",
        "datatype": "auto-detect",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": true,
        "rh": 0,
        "inputs": 0,
        "x": 1030,
        "y": 780,
        "wires": [
            [
                "05e8b547503632e9"
            ]
        ]
    },
    {
        "id": "78cbb95682b04311",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "",
        "group": "b605a216fbcede99",
        "order": 3,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "+m",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 1110,
        "y": 840,
        "wires": [
            [
                "620ddb748ccac48f"
            ]
        ]
    },
    {
        "id": "22a11c7a99773646",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "",
        "group": "b605a216fbcede99",
        "order": 6,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "-m",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 1110,
        "y": 900,
        "wires": [
            [
                "f68b0e444f0aa36c"
            ]
        ]
    },
    {
        "id": "01004d9502d79268",
        "type": "ui_text",
        "z": "07168438cc3bbe1a",
        "group": "b605a216fbcede99",
        "order": 2,
        "width": 0,
        "height": 0,
        "name": "",
        "label": "Time",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "className": "",
        "x": 1250,
        "y": 1200,
        "wires": []
    },
    {
        "id": "84a1bf78a296db63",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "",
        "group": "b605a216fbcede99",
        "order": 4,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "+s",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 1110,
        "y": 960,
        "wires": [
            [
                "0ae3a0a09fe21968"
            ]
        ]
    },
    {
        "id": "d5b511b1be15f903",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "",
        "group": "b605a216fbcede99",
        "order": 7,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "-s",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 1110,
        "y": 1020,
        "wires": [
            [
                "5838bcc7a46e1c1a"
            ]
        ]
    },
    {
        "id": "25db321673acc717",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "",
        "group": "b605a216fbcede99",
        "order": 5,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "+t",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 1110,
        "y": 1080,
        "wires": [
            [
                "a178521fc9a4af3f"
            ]
        ]
    },
    {
        "id": "ad27a08b4ecef42c",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "",
        "group": "b605a216fbcede99",
        "order": 8,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "-t",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 1110,
        "y": 1140,
        "wires": [
            [
                "dc8f353555a847f0"
            ]
        ]
    },
    {
        "id": "9bb925c07a94c89a",
        "type": "mqtt in",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/keyframeTimeString/R",
        "qos": "2",
        "datatype": "auto-detect",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": true,
        "rh": 0,
        "inputs": 0,
        "x": 1010,
        "y": 1200,
        "wires": [
            [
                "01004d9502d79268"
            ]
        ]
    },
    {
        "id": "620ddb748ccac48f",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/incMinutes/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1320,
        "y": 840,
        "wires": []
    },
    {
        "id": "f68b0e444f0aa36c",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/decMinutes/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1330,
        "y": 900,
        "wires": []
    },
    {
        "id": "0ae3a0a09fe21968",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/incSeconds/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1330,
        "y": 960,
        "wires": []
    },
    {
        "id": "5838bcc7a46e1c1a",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/decSeconds/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1330,
        "y": 1020,
        "wires": []
    },
    {
        "id": "a178521fc9a4af3f",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/incTenthSeconds/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1340,
        "y": 1080,
        "wires": []
    },
    {
        "id": "dc8f353555a847f0",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/decTenthSeconds/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1350,
        "y": 1140,
        "wires": []
    },
    {
        "id": "f82d610c0805552f",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "Play all",
        "group": "1631f553c2fec44c",
        "order": 8,
        "width": "2",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "play_arrow",
        "payload": "",
        "payloadType": "date",
        "topic": "topic",
        "topicType": "msg",
        "x": 380,
        "y": 1020,
        "wires": [
            [
                "224f76e82c1c1add"
            ]
        ]
    },
    {
        "id": "224f76e82c1c1add",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/playAllKeyframes/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 620,
        "y": 1020,
        "wires": []
    },
    {
        "id": "67b281fbcc2a54b8",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "Move to keyframepointer",
        "group": "1631f553c2fec44c",
        "order": 4,
        "width": "3",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "Move to keyframepointer",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "forward",
        "payload": "",
        "payloadType": "date",
        "topic": "topic",
        "topicType": "msg",
        "x": 330,
        "y": 1200,
        "wires": [
            [
                "105954bbe81134ad"
            ]
        ]
    },
    {
        "id": "105954bbe81134ad",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/moveToKeyframePointer/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 650,
        "y": 1200,
        "wires": []
    },
    {
        "id": "f823e134e8df4242",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "Move to time",
        "group": "1631f553c2fec44c",
        "order": 5,
        "width": "3",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "Move to time",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "av_timer",
        "payload": "",
        "payloadType": "date",
        "topic": "topic",
        "topicType": "msg",
        "x": 370,
        "y": 1260,
        "wires": [
            [
                "e06552b62fb78dad"
            ]
        ]
    },
    {
        "id": "e06552b62fb78dad",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/moveToTimePosition/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 640,
        "y": 1260,
        "wires": []
    },
    {
        "id": "9d10dd42462bea2f",
        "type": "ui_text",
        "z": "07168438cc3bbe1a",
        "group": "607f818ccaddea8f",
        "order": 4,
        "width": 0,
        "height": 0,
        "name": "",
        "label": "KF pointer",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "className": "",
        "x": 1270,
        "y": 720,
        "wires": []
    },
    {
        "id": "4d497af69d5ab8d1",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "First",
        "group": "1631f553c2fec44c",
        "order": 6,
        "width": "1",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "first_page",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 390,
        "y": 900,
        "wires": [
            [
                "3cf2bdd2de18f6a0"
            ]
        ]
    },
    {
        "id": "264ef548c942f160",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "Last",
        "group": "1631f553c2fec44c",
        "order": 10,
        "width": "1",
        "height": "1",
        "passthru": false,
        "label": "",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "last_page",
        "payload": "",
        "payloadType": "str",
        "topic": "topic",
        "topicType": "msg",
        "x": 390,
        "y": 1140,
        "wires": [
            [
                "61ac2db028583d83"
            ]
        ]
    },
    {
        "id": "3cf2bdd2de18f6a0",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/setKeyframepointerToMin/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 650,
        "y": 900,
        "wires": []
    },
    {
        "id": "61ac2db028583d83",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "keyframeEditor/setKeyframepointerToMax/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 650,
        "y": 1140,
        "wires": []
    },
    {
        "id": "570112087202d73e",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/home/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1350,
        "y": 420,
        "wires": []
    },
    {
        "id": "7a295ef5d1d5acd7",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "",
        "group": "38ff8262509bdeed",
        "order": 6,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Homing",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "topicType": "str",
        "x": 1080,
        "y": 420,
        "wires": [
            [
                "570112087202d73e"
            ]
        ]
    },
    {
        "id": "98ffa4bab5310209",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/killAlarmLock/W",
        "qos": "",
        "retain": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1370,
        "y": 300,
        "wires": []
    },
    {
        "id": "f9fca64405168641",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "",
        "group": "38ff8262509bdeed",
        "order": 1,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Kill Alarm",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "topicType": "str",
        "x": 1080,
        "y": 300,
        "wires": [
            [
                "98ffa4bab5310209"
            ]
        ]
    },
    {
        "id": "9c349ef9da35a2c9",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/softReset/W",
        "qos": "",
        "retain": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1360,
        "y": 360,
        "wires": []
    },
    {
        "id": "9a50b66f0553f245",
        "type": "ui_button",
        "z": "07168438cc3bbe1a",
        "name": "",
        "group": "38ff8262509bdeed",
        "order": 2,
        "width": 0,
        "height": 0,
        "passthru": false,
        "label": "Soft Reset",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "className": "",
        "icon": "",
        "payload": "",
        "payloadType": "str",
        "topic": "",
        "topicType": "str",
        "x": 1070,
        "y": 360,
        "wires": [
            [
                "9c349ef9da35a2c9"
            ]
        ]
    },
    {
        "id": "ebde2ddb1a442033",
        "type": "ui_joystick",
        "z": "07168438cc3bbe1a",
        "name": "Right Stick",
        "group": "98a790d19ee0eaef",
        "order": 1,
        "width": "6",
        "height": "5",
        "trigger": "all",
        "timeInterval": "200",
        "useThemeColor": true,
        "color": "#000000",
        "threshold": 0.1,
        "directions": "all",
        "shape": "circle",
        "centerAtRelease": true,
        "x": 320,
        "y": 360,
        "wires": [
            []
        ]
    },
    {
        "id": "a2f06ea23f3688e0",
        "type": "mqtt in",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/status/R",
        "qos": "2",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "inputs": 0,
        "x": 1040,
        "y": 660,
        "wires": [
            [
                "0e5459fcbc8d4f1f",
                "1bebf054e0a4d821"
            ]
        ]
    },
    {
        "id": "0e5459fcbc8d4f1f",
        "type": "ui_text",
        "z": "07168438cc3bbe1a",
        "group": "607f818ccaddea8f",
        "order": 1,
        "width": 5,
        "height": 1,
        "name": "",
        "label": "Status",
        "format": "{{msg.payload}}",
        "layout": "row-spread",
        "className": "",
        "x": 1260,
        "y": 660,
        "wires": []
    },
    {
        "id": "1bebf054e0a4d821",
        "type": "ui_led",
        "z": "07168438cc3bbe1a",
        "order": 2,
        "group": "607f818ccaddea8f",
        "width": "1",
        "height": 1,
        "label": "",
        "labelPlacement": "left",
        "labelAlignment": "left",
        "colorForValue": [
            {
                "color": "#008000",
                "value": "Idle",
                "valueType": "str"
            },
            {
                "color": "#ffff00",
                "value": "Jog",
                "valueType": "str"
            },
            {
                "color": "#0000ff",
                "value": "Run",
                "valueType": "str"
            },
            {
                "color": "#ff0000",
                "value": "Alarm",
                "valueType": "str"
            }
        ],
        "allowColorForValueInMessage": false,
        "shape": "circle",
        "showGlow": true,
        "name": "Status",
        "x": 1540,
        "y": 660,
        "wires": []
    },
    {
        "id": "3e8e594609333fbf",
        "type": "ui_joystick",
        "z": "07168438cc3bbe1a",
        "name": "Left Trigger",
        "group": "5654c0f3536c995a",
        "order": 2,
        "width": "6",
        "height": "2",
        "trigger": "all",
        "timeInterval": "200",
        "useThemeColor": true,
        "color": "#000000",
        "threshold": 0.1,
        "directions": "hor",
        "shape": "circle",
        "centerAtRelease": true,
        "x": 310,
        "y": 240,
        "wires": [
            []
        ]
    },
    {
        "id": "56c685c7ef8cc2c6",
        "type": "ui_joystick",
        "z": "07168438cc3bbe1a",
        "name": "Right Trigger",
        "group": "98a790d19ee0eaef",
        "order": 2,
        "width": "6",
        "height": "2",
        "trigger": "all",
        "timeInterval": "200",
        "useThemeColor": true,
        "color": "#000000",
        "threshold": 0.1,
        "directions": "hor",
        "shape": "circle",
        "centerAtRelease": true,
        "x": 310,
        "y": 420,
        "wires": [
            []
        ]
    },
    {
        "id": "29a437c905745384",
        "type": "ui_slider",
        "z": "07168438cc3bbe1a",
        "name": "",
        "label": "Max jog",
        "tooltip": "",
        "group": "c23deb21fe5d2555",
        "order": 1,
        "width": "6",
        "height": "1",
        "passthru": false,
        "outs": "end",
        "topic": "",
        "topicType": "str",
        "min": "750",
        "max": "10000",
        "step": "50",
        "className": "",
        "x": 1260,
        "y": 140,
        "wires": [
            [
                "dc124f856cfd7493"
            ]
        ]
    },
    {
        "id": "d5925427a2f236ce",
        "type": "mqtt in",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/maxJogSpeed/R",
        "qos": "0",
        "datatype": "auto",
        "broker": "b2bea16b.4dd8d",
        "nl": false,
        "rap": false,
        "inputs": 0,
        "x": 990,
        "y": 140,
        "wires": [
            [
                "29a437c905745384"
            ]
        ]
    },
    {
        "id": "dc124f856cfd7493",
        "type": "mqtt out",
        "z": "07168438cc3bbe1a",
        "name": "",
        "topic": "interface/panTiltSlider/maxJogSpeed/W",
        "qos": "",
        "retain": "",
        "respTopic": "",
        "contentType": "",
        "userProps": "",
        "correl": "",
        "expiry": "",
        "broker": "b2bea16b.4dd8d",
        "x": 1540,
        "y": 140,
        "wires": []
    }
]
