var console = require('./debug.js')
const Axis = require("./axis.js")
var statusStore = require('./mqttStatus.js').statusStore
var settingStore = require('./mqttSettings.js').settingStore
var functionStore = require('./mqttFunctions.js').functionStore

const INTERFACE_BASETOPIC = require("./mqttBasetopics.js").INTERFACE_BASETOPIC

class Interface {
	constructor(options) {
		console.group("new Interface: " + options.name)
		this.name = options.name
		if (options.abbreviation == undefined) {
			options.abbreviation = options.name
		}
		this.abbreviation = options.abbreviation
		this.basetopic = INTERFACE_BASETOPIC + "/" + options.name
		this.axes = []

		// functions required by the Interface class
		// these functions must be defined by the child class

		// function to move a single axis
		// argument : {
		// 		name: the name of the axis
		// 		value: the value to which it should move
		// }
		this.moveSingleAxisFunction = options.moveSingleAxisFunction
		
		// 
		// argument : {
		// 		axes : array of axis names 
		// 		duration : the duration of the move
		// }
		// 
		this.createKeyframeCode = options.createKeyframeCode
		this.pause = options.pause
		this.play = options.play
		this.addToCommandBuffer = options.addToCommandBuffer
		
		this.status = statusStore.addStatus({
			name: this.basetopic + "/status",
			value: "Idle",
			topic: this.basetopic + "/status"
		})
		this.maxJogSpeed = settingStore.addSetting({
			name: this.basetopic + "/maxJogSpeed",
			value: options.maxJogSpeed,
			callback: undefined,
			topic: this.basetopic + "/maxJogSpeed"
		})
		console.log("register interface moveTo function");
		this.moveTo = functionStore.addFunction({
			name: this.basetopic + "/moveTo",
			callback: options.moveToFunction,
			topic: this.basetopic + "/moveTo"
		})

		if (options.statusCallback != undefined) {
			console.log("add statusCallback");
			console.log(this.name);
			this.status.addCallback(options.statusCallback)
		}

		// settingsCallback will be called when a setting has changed.
		// this has been created to update the gui when a setting has been changed over mqtt
		if (options.settingsCallback != undefined) {
			console.log("add settingsCallback");
			console.log(this.name);
			this.maxJogSpeed.addCallback(options.settingsCallback)
		}

		console.groupEnd()
	}
	addAxis(axis) {
		console.log("addAxis to interface: " + this.name);
		if (this.axes[axis.axisName] == undefined) {
			axis.interface = this
			axis.moveTo = this.moveSingleAxisFunction
			this.axes[axis.axisName] = new Axis(axis)
			return this.axes[axis.axisName]
		} else {
			console.log(this);
			console.error("AXIS ALREADY DEFINED!");
		}
	}
	getAxis(axis) {
		try {
			return this.axes[axis.axisName]
		} catch (e) {
			console.error("AXIS NOT DEFIEND!");
			console.error(e);
		}
	}
	getAxesVector() {
		var vector = {
			name: this.name,
			axes: {}
		}
		Object.entries(this.axes).forEach(([axisName, axis]) => {
			vector.axes[axis.axisName] = axis.getData()
		});
		return vector
	}
}

module.exports = Interface
