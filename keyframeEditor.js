var console = require('./debug.js')
var functionStore = require('./mqttFunctions.js').functionStore
var statusStore = require('./mqttStatus.js').statusStore
var settingStore = require('./mqttSettings.js').settingStore
var mqtt = require("./mqttHandler.js").mqttHandler

const fs = require('fs')
const execSync = require("child_process").execSync

const KEYFRAME_FILENAME = "keyframes.json"
const KEYFRAME_EDITOR_BASETOPIC = require("./mqttBasetopics.js").KEYFRAME_EDITOR_BASETOPIC

class KeyframeEditor {
	constructor() {
		console.group("KeyframeEditor constructor");
		var self = this;
		this.interfaces = []
		this.timeEditPosition = 1; //0:tenthSeconds; 1:seconds; 2:minutes
		this.timeEditPositionStrings = ["t", "s", "m"]
		this.keyframes = [];
		this.allInterfacesIdleCallback = undefined
		this.mode = "";
		this.modes = {
			add: "ADD",
			empty: ""
		}

		this.keyframePointerStatus = statusStore.addStatus({
			name: KEYFRAME_EDITOR_BASETOPIC + "/keyframePointer",
			value: this.keyframes.length - 1,
			setter: (value) => {
				if (this.keyframes.length > 0) {
					if (value < 0) {
						value = 0;
					} else if (value > this.keyframes.length - 1) {
						value = this.keyframes.length - 1
					}
				} else {
					value = -1
				}
				return value
			},
			callback: (value) => {
				if (this.keyframes.length > 0) {
					this.timeValueSetting.update(this.keyframes[this.keyframePointerStatus.value].t);
				}
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/keyframePointer"
		})

		this.keyframeCountStatus = statusStore.addStatus({
			name: KEYFRAME_EDITOR_BASETOPIC + "/keyframeCount",
			value: this.keyframes.length,
			topic: KEYFRAME_EDITOR_BASETOPIC + "/keyframeCount"
		})

		this.keyframesStatus = statusStore.addStatus({
			name: KEYFRAME_EDITOR_BASETOPIC + "/keyframes",
			value: JSON.stringify(this.keyframes),
			topic: KEYFRAME_EDITOR_BASETOPIC + "/keyframes"
		})

		this.timeValueSetting = settingStore.addSetting({
			name: KEYFRAME_EDITOR_BASETOPIC + "/timeValue",
			value: 0,
			callback: (value) => {
				this.updateMqttTimeStrings()
				this.setKeyframePointerFromTime()
			},
			setter: (value) => {
				if (value < 0) {
					value = 0
				}
				return value
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/timeValue"
		})

		if (this.keyframes.length > 0) {
			this.timeValueSetting.update(this.keyframes[this.keyframes.length - 1])
		}

		// 00:00.0 <= Display the time
		this.keyframeTimeStringStatus = statusStore.addStatus({
			name: KEYFRAME_EDITOR_BASETOPIC + "/keyframeTimeString",
			value: this.getKeyframeTimeString(),
			topic: KEYFRAME_EDITOR_BASETOPIC + "/keyframeTimeString"
		})

		// 00s00.0 <= Display the time and indicate which digit will be edited
		this.keyframeTimeEditStringStatus = statusStore.addStatus({
			name: KEYFRAME_EDITOR_BASETOPIC + "/keyframeTimeEditString",
			value: this.getKeyframeTimeEditString(),
			topic: KEYFRAME_EDITOR_BASETOPIC + "/keyframeTimeEditString"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/playAllKeyframes",
			callback: (value) => {
				console.info("PLAY ALL KEYFRAMES");
				this.playAllKeyframes()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/playAllKeyframes"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/setKeyframes",
			callback: (value) => {
				console.info("SET KEYFRAMES");
				var buf = Buffer.from(value)
				this.setKeyframesFromString(buf.toString())
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/setKeyframes"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/decMinutes",
			callback: (value) => {
				console.info("decMinutes");
				this.decMinutes()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/decMinutes"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/incMinutes",
			callback: (value) => {
				console.info("incMinutes");
				this.incMinutes()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/incMinutes"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/decSeconds",
			callback: (value) => {
				console.info("decSeconds");
				this.decSeconds()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/decSeconds"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/incSeconds",
			callback: (value) => {
				console.info("incSeconds");
				this.incSeconds()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/incSeconds"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/decTenthSeconds",
			callback: (value) => {
				console.info("decTenthSeconds");
				this.decTenthSeconds()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/decTenthSeconds"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/incTenthSeconds",
			callback: (value) => {
				console.info("incTenthSeconds");
				this.incTenthSeconds()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/incTenthSeconds"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/decTime",
			callback: (value) => {
				console.info("Decrement time");
				this.decTime()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/decTime"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/incTime",
			callback: (value) => {
				console.info("Increment time");
				this.incTime()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/incTime"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/decTimeEditPosition",
			callback: (value) => {
				console.info("DecTimeEditPosition");
				this.decTimeEditPosition()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/decTimeEditPosition"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/incTimeEditPosition",
			callback: (value) => {
				console.info("IncTimeEditPosition");
				this.incTimeEditPosition()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/incTimeEditPosition"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/incKeyframePointer",
			callback: (value) => {
				console.info("incKeyframePointer");
				this.incKeyframePointer()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/incKeyframePointer"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/decKeyframePointer",
			callback: (value) => {
				console.info("decKeyframePointer");
				this.decKeyframePointer()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/decKeyframePointer"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/setKeyframepointerToMin",
			callback: (value) => {
				console.info("setKeyframepointerToMin");
				this.setKeyframepointerToMin()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/setKeyframepointerToMin"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/setKeyframepointerToMax",
			callback: (value) => {
				console.info("setKeyframepointerToMax");
				this.setKeyframepointerToMax()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/setKeyframepointerToMax"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/moveToKeyframePointer",
			callback: (value) => {
				console.info("moveToKeyframePointer");
				this.moveToKeyframePointer()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/moveToKeyframePointer"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/moveToTimePosition",
			callback: (value) => {
				console.info("moveToTimePosition");
				this.moveToTimePosition()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/moveToTimePosition"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/addKeyframe",
			callback: (value) => {
				console.info("addKeyframe");
				this.addKeyframe(value)
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/addKeyframe"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/clearAllKeyframes",
			callback: (value) => {
				console.info("clearAllKeyframes");
				this.clearAllKeyframes(value)
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/clearAllKeyframes"
		})

		functionStore.addFunction({
			name: KEYFRAME_EDITOR_BASETOPIC + "/deleteKeyframe",
			callback: (value) => {
				console.info("deleteKeyframe");
				this.deleteKeyframe()
			},
			topic: KEYFRAME_EDITOR_BASETOPIC + "/deleteKeyframe"
		})

		// Setup keyframe file
		console.log("Setup keyframe file")
		try {
			const data = fs.readFileSync(KEYFRAME_FILENAME, 'utf8')
			console.info("Keyframes file does exist.")
			console.info("Restoring keyframes...")
			try {
				var loadedKeyframes = JSON.parse(data);
				loadedKeyframes.forEach((newKeyframe) => {
					this.keyframes.push(new Keyframe(newKeyframe))
				});
				this.keyframesStatus.update(JSON.stringify(this.keyframes))
				this.keyframePointerStatus.update(this.keyframes.length - 1)
				this.keyframeCountStatus.update(this.keyframes.length)
				if (this.keyframes.length > 0) {
					this.timeValueSetting.update(this.keyframes[this.keyframes.length - 1].t)
				}
				console.info("done.");
			} catch (e) {
				console.error(e);
				console.info("Keyframes file is corrupted.")
				console.info("Creating new file...")
				this.saveKeyframes()
				console.info("done.");
			}
		} catch (err) {
			console.info("Keyframes file does not exist.")
			console.info("Creating file...")
			this.saveKeyframes()
			console.info("done.");
		}

		// update all interfaces
		console.log("update all interfaces");
		for (const [key, value] of Object.entries(this.interfaces)) {
			var interfaceInstance = value
			try {
				interfaceInstance.statusQuery()
			} catch (e) {}
		};
		console.groupEnd()
	} //end of constructor

	//****************************************************************************************************************************************************************************************************
	// Functions


	/**
	 * Add a new interface to the list of interfaces
	 * @param {[type]} newInterface  [description]
	 */
	addInterface(newInterface) {
		// check if name exists
		if (this.interfaces[newInterface.name] == undefined) {
			this.interfaces[newInterface.name] = newInterface
			this.interfaces[newInterface.name].status.addCallback(() => {
				if (this.allInterfacesIdleCallback != undefined) {
					if (this.allInterfacesAreIdle()) {
						this.allInterfacesIdleCallback();
					}
				}
			})
			return this.interfaces[newInterface.name]
		} else {
			console.error("Interface already exists: " + newInterface.name);
		}
	}


	/**
	 * Clear all keyframes in the list and the keyframes.json file
	 * @return {[type]} [description]
	 */
	clearAllKeyframes() {
		console.group("clearAllKeyframes")
		this.keyframes = [];
		this.keyframePointerStatus.update(-1)
		this.saveKeyframes()
		console.groupEnd()
	}

	/**
	 * add keyframes to the existing keyframes list and file.
	 * @param {[type]} string  to be interpreted as keyframes in JSON format
	 */
	addKeyframesFromString(string) {
		console.group("addKeyframesFromString")
		var loadedKeyframes = JSON.parse(string);
		loadedKeyframes.forEach((newKeyframe) => {
			this.keyframes.push(new Keyframe(newKeyframe))
			this.keyframeCountStatus.update(this.keyframes.length)
			this.keyframePointerStatus.update(this.keyframePointerStatus.value + 1)
		});
		console.groupEnd()
	}


	/**
	 * overwrite the keyframes in the list and json file
	 * @param {[type]} string  to be interpreted as keyframes in JSON format
	 */
	setKeyframesFromString(string) {
		console.group("setKeyframesFromString")
		this.clearAllKeyframes()
		this.addKeyframesFromString(string)
		this.sortKeyframes()
		this.updateAllGcodes()
		this.saveKeyframes()
		console.groupEnd()
	}

	/**
	 * [allInterfacesAreIdle]
	 * check if all interfaces are in the state idle
	 * @return {bool} true if all interfaces are idle
	 */
	allInterfacesAreIdle() {
		for (const [name, interfaceInstance] of Object.entries(this.interfaces)) {
			if (interfaceInstance.status.value != "Idle") {
				return false
			}
		}
		return true
	}

	/**
	 * [toggleInclude]
	 * toggles the include state of the specified axis of the specified interface
	 * @param  {string}  interfaceName 	the name of the interface
	 * @param  {string} axisName      	the name of the axis
	 */
	toggleInclude(interfaceName, axisName) {
		this.interfaces[interfaceName].axes[axisName].toggleInclude.callFunction()
	}

	/**
	 * [jog]
	 * execute the jog function of the specified interface instance
	 * @param  {string} interfaceInstance name of the interface instance to jog
	 * @param  {object} coordinates object that contains the axis names and jogfactors
	 * 								of the specified interface to jog to
	 */
	jog(interfaceInstance, coordinates) {
		try {
			this.interfaces[interfaceInstance].jog(coordinates)
		} catch (e) {

		}
	}

	/**
	 * [jogCancel]
	 * execute the jogCancel function of the specified interface instance
	 * @param  {string} interfaceInstance name of the interface instance to jogCancel
	 */
	jogCancel(interfaceInstance) {
		try {
			this.interfaces[interfaceInstance].jogCancel()
		} catch (e) {

		}
	}

	/**
	 * [softReset]
	 * execute the softReset function of the specified interface instance
	 * @param  {string} interfaceInstance name of the interface instance to softReset
	 */
	softReset(interfaceInstance) {
		try {
			this.interfaces[interfaceInstance].softReset()
		} catch (e) {

		}
	}



	// TODO:
	/**
	 * [setZero]
	 * execute the setZero function of the specified interface instance
	 * @param  {string} interfaceInstance name of the interface instance to setZero
	 */
	setZero(interfaceInstance) {
		try {
			this.interfaces[interfaceInstance].setZero()
		} catch (e) {

		}
	}

	/**
	 * [killAlarmLock]
	 * execute the killAlarmLock function of the specified interface instance
	 * @param  {string} interfaceInstance name of the interface instance to killAlarmLock
	 */
	killAlarmLock(interfaceInstance) {
		try {
			this.interfaces[interfaceInstance].killAlarmLock()
		} catch (e) {

		}
	}

	/**
	 * [home]
	 * execute the home function of the specified interface instance
	 * @param  {string} interfaceInstance name of the interface instance to home
	 */
	home(interfaceInstance) {
		try {
			this.interfaces[interfaceInstance].home()
		} catch (e) {

		}
	}


	/**
	 * [playAllKeyframes]
	 * moves all interfaces to the position of the first keyframe,
	 * waits for all interfaces to arrive,
	 * waits for one second
	 * and then plays all keyframes
	 */
	playAllKeyframes() {
		console.info("Play all keyframes");
		if (this.keyframes.length < 1) {
			return;
		}

		this.sortKeyframes()
		this.updateAllGcodes()

		console.info("Move axis to start");
		// move all interfaces to the position of the first keyframe
		for (const [name, interfaceVector] of Object.entries(this.keyframes[0].interfaces)) {
			this.interfaces[name].play()
			this.interfaces[name].addToCommandBuffer('G94\r');
			this.interfaces[name].addToCommandBuffer('G90\r');
			this.interfaces[name].addToCommandBuffer(this.interfaces[name].createKeyframeCode(interfaceVector.axes, 0))
		}

		console.info("Wait for all axes to finish");

		// wait until all axes are idle
		this.allInterfacesIdleCallback = () => {
			console.info("allInterfacesIdleCallback");
			this.beep()
			var i = 0
			for (const [name, keyframe] of Object.entries(this.keyframes)) {
				for (const [name, interfaceVector] of Object.entries(keyframe.interfaces)) {
					if (i == 0) {
						this.interfaces[name].pause()
						this.interfaces[name].addToCommandBuffer('G94\r');
						this.interfaces[name].addToCommandBuffer('G90\r');
						this.interfaces[name].addToCommandBuffer("G4P1\r");
					} else {
						this.interfaces[name].addToCommandBuffer(interfaceVector.code)
					}
				};
				i++
			};

			for (const [name, interfaceInstance] of Object.entries(this.interfaces)) {
				interfaceInstance.play()
			}
			this.allInterfacesIdleCallback = undefined;
		}

		// if all interfaces are already at start the status won't change
		// must happen after this function is left because we must let grbl do its thing first
		// TODO: no timeouts!! use EventEmitter and once
		setTimeout(() => {
			console.info("timeout reached: " + this.allInterfacesAreIdle());
			if (this.allInterfacesAreIdle()) {
				if (this.allInterfacesIdleCallback != undefined) {
					this.allInterfacesIdleCallback()
				}
			}
		}, 200)
	}

	/**
	 * execute a beep on the connected piezo buzzer
	 */
	beep() {
		try {
			execSync("sudo node beep.js", (err, std) => {
				console.info(err);
				console.info(std);
			})
		} catch (e) {
			console.error("Could not beep");
		}
	}

	/**
	 * [setModeAdd]
	 * sets the mode of the keyframeEditor to add
	 */
	setModeAdd() {
		this.mode = this.modes.add;
	}

	/**
	 * [resetMode]
	 * sets the mode of the keyframeEditor to empty
	 */
	resetMode() {
		this.mode = this.modes.empty;
	}

	/**
	 * creates a keyframe based on the current time value of the keyframeEditor
	 * the data value (e.g. position) is calculated based on the neighboring keyframes
	 * this takes includet and excludet axis into account and calculates the value accordingly
	 * @return {[type]} the newly created keyframe
	 */
	getKeyframeFromTime() {
		console.group("getKeyframeFromTime")
		var tempKeyframe = new Keyframe({
			interfaces: {},
			t: 0
		})

		// check if the keyframe at the current keyframePointer and
		// the keyframe after that one exists
		if (this.keyframes[this.keyframePointerStatus.value] != undefined &&
			this.keyframes[this.keyframePointerStatus.value + 1] != undefined) {
			console.log("Time is between keyframes");
			// if one axis is not included calculate its value based on the
			// position in time between this and the next keyframe
			for (const [name, interfaceInstance] of Object.entries(this.interfaces)) {
				for (const [axisName, axis] of Object.entries(interfaceInstance.axes)) {
					if (!axis.isIncluded()) {

						// find the same axis in the previous keyframe
						// this will always find a value because the first keyframe must contain all axis
						var valueA = undefined //previous keyframe
						var timeA = undefined //previous keyframe
						for (var i = this.keyframePointerStatus.value; i > -1; i--) {
							if (this.keyframes[i].interfaces[interfaceInstance.name].axes[axis.axisName].include) {
								valueA = this.keyframes[i].interfaces[interfaceInstance.name].axes[axis.axisName].value
								timeA = this.keyframes[i].t
							}
						}

						var valueC = undefined //next keyframe
						var timeC = undefined //next keyframe
						for (var i = this.keyframePointerStatus.value + 1; i < this.keyframes.length; i++) {
							if (this.keyframes[i].interfaces[interfaceInstance.name].axes[axis.axisName].include) {
								valueC = this.keyframes[i].interfaces[interfaceInstance.name].axes[axis.axisName].value
								timeC = this.keyframes[i].t
							}
						}

						// The last keyframe
						if (valueC == undefined && timeC == undefined) {
							// TODO:
							// handle case of axis not includet in all keyframes after the keyframePointer
							console.error("axis not includet in all keyframes after the keyframePointer");
						}

						// if both keyframes were found calculate the value of the coordinate
						// based on the position in time between the two keyframes
						if (valueA != undefined && timeA != undefined && valueC != undefined && timeC != undefined) {
							if (tempKeyframe.interfaces[interfaceInstance.name] == undefined) {
								tempKeyframe.interfaces[interfaceInstance.name] = {
									name: interfaceInstance.name,
									axes: {}
								}
							}

							// fill in temp keyframe
							// copy axis from existing keyframe
							tempKeyframe.interfaces[interfaceInstance.name].axes[axis.axisName] = {}
							Object.assign(tempKeyframe.interfaces[interfaceInstance.name].axes[axis.axisName],
								this.keyframes[0].interfaces[interfaceInstance.name].axes[axis.axisName])

							// replace the value with the calculated value
							// the value is calculated based of the current time position of the keyframeEditor
							tempKeyframe.interfaces[interfaceInstance.name].axes[axis.axisName].value =
								(valueA + ((valueC - valueA) * ((this.timeValueSetting.value - timeA) / (timeC - timeA))))
						} else {
							console.trace("PARTNER KEYFRAME NOT FOUND");
							console.info("valueA: " + valueA)
							console.info("timeA: " + timeA)
							console.info("valueC: " + valueC)
							console.info("timeC: " + timeC)
							console.info("interfaceInstance.name: " + interfaceInstance.name);
							console.info("axis.axisName: " + axis.axisName);
						}
					}
				}
			}
		}
		console.log(tempKeyframe);
		console.groupEnd();
		return tempKeyframe
	}

	/**
	 * [moveToKeyframe]
	 * moves all interfaces to the positions definde in the passed keyframe
	 * @param  {[type]} keyframe [description]
	 * @return {[type]}          [description]
	 */
	/**
	 * [moveToKeyframe description]
	 * @param  {[type]} keyframe	the keyframe instance to move to
	 * @param  {[type]} duration	the duration for the move to take (unit 0.1s) pass 0 to move at max speed
	 * @return {[type]}          [description]
	 */
	moveToKeyframe(keyframe, duration) {
		console.group("moveToKeyframe")
		if (duration == undefined) {
			duration = keyframe.t
		}
		console.log("keyframe: " + JSON.stringify(keyframe))
		console.log("duration: " + duration)
		for (const [name, interfaceVector] of Object.entries(keyframe.interfaces)) {
			this.interfaces[name].play()
			this.interfaces[name].addToCommandBuffer('G94\r');
			this.interfaces[name].addToCommandBuffer('G90\r');
			this.interfaces[name].addToCommandBuffer(this.interfaces[name].createKeyframeCode(interfaceVector.axes, duration))
		}
		console.groupEnd()
	}

	/**
	 * Move all axis to the keyframe the keyframePointer points to
	 * the move is at max speed
	 * @return {[type]} [description]
	 */
	moveToKeyframePointer() {
		console.group("moveToKeyframePointer")
		if (this.keyframes[this.keyframePointerStatus.value] != undefined) {
			this.moveToKeyframe(this.keyframes[this.keyframePointerStatus.value], 0)
		} else {
			console.log("No keyframes defined")
		}
		console.groupEnd()
	}

	moveToTimePosition() {
		console.group("moveToTimePosition")
		this.moveToKeyframe(this.getKeyframeFromTime())
		console.groupEnd()
	}

	/**
	 * [playKeyframeAtPosition]
	 * moves all interfaces to the keyframe before the keyframe at the keyframePointer position,
	 * then plays the keyframe at the keyframePointer position
	 * @return {[type]} [description]
	 */
	playKeyframeAtPosition() {
		console.info("Play keyframe at position");
		if (this.keyframes.length < 1) {
			return;
		}

		// the keyframe previous to the keyframePointer must exist because we need a point to start from
		if (this.keyframes[this.keyframePointerStatus.value - 1] == undefined) {
			return;
		}
		console.info("Move axis to start");
		// move all interfaces to the position of the first keyframe
		for (const [name, interfaceVector] of Object.entries(this.keyframes[this.keyframePointerStatus.value - 1].interfaces)) {
			this.interfaces[name].play()
			this.interfaces[name].addToCommandBuffer('G94\r');
			this.interfaces[name].addToCommandBuffer('G90\r');
			this.interfaces[name].addToCommandBuffer(this.interfaces[name].createKeyframeCode(interfaceVector.axes, 0))
		}

		console.info("Wait for all axes to finish");


		// TODO: remodel to use pormises
		// wait until all axes are idle
		this.allInterfacesIdleCallback = () => {
			console.info("allInterfacesIdleCallback");
			for (const [name, interfaceVector] of Object.entries(this.keyframes[this.keyframePointerStatus.value].interfaces)) {
				this.interfaces[name].pause()
				this.interfaces[name].addToCommandBuffer('G94\r');
				this.interfaces[name].addToCommandBuffer('G90\r');
				this.interfaces[name].addToCommandBuffer("G4P1\r");
				this.interfaces[name].addToCommandBuffer(interfaceVector.code)
			};

			for (const [name, interfaceInstance] of Object.entries(this.interfaces)) {
				interfaceInstance.play()
			}
			this.allInterfacesIdleCallback = undefined;
		}

		// if all interfaces are already at start the status won't change
		// must happen after this function is left because we must let grbl do its thing first
		setTimeout(() => {
			console.info("timeout reached: " + this.allInterfacesAreIdle());
			if (this.allInterfacesAreIdle()) {
				if (allInterfacesIdleCallback != undefined) {
					this.allInterfacesIdleCallback()
				}
			}
		}, 200)
	}

	/**
	 * [addKeyframe]
	 * adds a keyframe based on the current position of all interfaces
	 * keyframe is added after current keyframePointerStatus
	 */
	addKeyframe() {
		console.group("addKeyframe");

		// set t = 0 if first keyframe
		var t = this.keyframes.length > 0 ? this.timeValueSetting.value : 0;
		

		var interfaces = {}

		for (const [key, value] of Object.entries(this.interfaces)) {
			var interfaceInstance = value
			interfaces[interfaceInstance.name] = interfaceInstance.getAxesVector()
			interfaces[interfaceInstance.name].abbreviation = interfaceInstance.abbreviation
		}
		this.keyframes.splice(this.keyframePointerStatus.value + 1, 0, new Keyframe({
			interfaces: interfaces,
			t: t
		}))
		this.keyframeCountStatus.update(this.keyframes.length)
		this.keyframePointerStatus.update(this.keyframePointerStatus.value + 1)

		this.sortKeyframes()
		this.updateAllGcodes()
		this.saveKeyframes()

		console.groupEnd()
	}

	/**
	 * [deleteKeyframe]
	 * deletes the keyframe at the current position of the keyframePointer
	 * or the provided position.
	 * corrects the keyframePointer if it is out of bounds.
	 * enshures that the first keyframe is always at t=0
	 * @return {[type]}     [description]
	 */
	deleteKeyframe() {
		console.group("deleteKeyframe")
		if (this.keyframes.length < 1) {
			console.groupEnd()
			return;
		}
		this.keyframes.splice(this.keyframePointerStatus.value, 1)
		if (this.keyframes.length > 0) {
			if (this.keyframes[0].t > 0) {
				var time = this.keyframes[0].t
				this.keyframes.forEach((keyframe) => {
					keyframe.t -= time
				})
			}
		}
		if (this.keyframePointerStatus.value > this.keyframes.length - 1) {
			this.keyframePointerStatus.value = this.keyframes.length - 1
		}
		this.saveKeyframes()
		console.groupEnd()
	}

	/**
	 * [saveKeyframes]
	 * saves the keyframes to a file at KEYFRAME_FILENAME
	 * @return {[type]} [description]
	 */
	saveKeyframes() {
		console.group("saveKeyframes");
		fs.writeFile(KEYFRAME_FILENAME, JSON.stringify(this.keyframes), err => {
			if (err) {
				console.error("Problem writing Keyframes.")
				console.error(err)
				return
			}
		})

		if (this.keyframeCountStatus != undefined) {
			this.keyframeCountStatus.update(this.keyframes.length)
		}

		if (this.keyframesStatus != undefined) {
			this.keyframesStatus.update(JSON.stringify(this.keyframes))
		}
		console.groupEnd()
	}

	/**
	 * [incKeyframePointer]
	 * increments the keyframe pointer by 1
	 * keeps the pointer within the bounds of the keyframes array
	 * @return {[type]} [description]
	 */
	incKeyframePointer() {
		console.group("incKeyframePointer")
		this.keyframePointerStatus.update(this.keyframePointerStatus.value + 1)
		console.groupEnd()
	}

	/**
	 * [decKeyframePointer]
	 * decrements the keyframe pointer by 1
	 * keeps the pointer within the bounds of the keyframes array
	 * @return {[type]} [description]
	 */
	decKeyframePointer() {
		console.group("decKeyframePointer")
		this.keyframePointerStatus.update(this.keyframePointerStatus.value - 1)
		console.groupEnd()
	}

	/**
	 * [setKeyframepointerToMin]
	 * sets the keyframePointer to the minimum value
	 * if no keyframes are present it is set to -1
	 */
	setKeyframepointerToMin() {
		if (this.keyframes.length > 0) {
			this.keyframePointerStatus.update(0)
		} else {
			this.keyframePointerStatus.update(-1)
		}
	}

	/**
	 * [setKeyframepointerToMax]
	 * sets the keyframePointer to the maximum value
	 * if no keyframes are present it is set to -1
	 */
	setKeyframepointerToMax() {
		if (this.keyframes.length > 0) {
			this.keyframePointerStatus.update(this.keyframes.length - 1)
		} else {
			this.keyframePointerStatus.update(-1)
		}
	}

	/**
	 * Helper function to update all mqtt time settings/statuses
	 * when a local change occures
	 * @return {[type]} [description]
	 */
	updateMqttTimeStrings() {
		console.group("updateMqttTimeStrings")
		try {
			this.keyframeTimeStringStatus.update(this.getKeyframeTimeString())
			this.keyframeTimeEditStringStatus.update(this.getKeyframeTimeEditString())
		} catch (e) {}
		console.groupEnd()
	}


	getTime() {
		return {
			tenthSeconds: this.timeValueSetting.value % 10,
			seconds: Math.floor(this.timeValueSetting.value / 10) % 60,
			minutes: Math.floor(this.timeValueSetting.value / 600)
		}
	}


	setKeyframePointerFromTime() {
		// TODO: Why try catch?
		try {
			if (this.keyframes.length < 1) {
				this.keyframePointerStatus.update(-1)
				return
			}

			if (this.timeValueSetting.value == 0 && this.keyframes.length > 0) {
				this.keyframePointerStatus.update(0)
				return
			}
			// time is after last keyframe
			if (this.timeValueSetting.value >= this.keyframes[this.keyframes.length - 1].t) {
				this.keyframePointerStatus.update(this.keyframes.length - 1)
				return
			}
			// time is between keyframes
			this.keyframes.forEach((keyframe, i) => {
				// check if next keyframe exists
				if (this.keyframes[i + 1] != undefined) {
					// check if the timeValue is between keyframes
					if (this.timeValueSetting.value >= keyframe.t && this.timeValueSetting.value < this.keyframes[i + 1].t) {
						this.keyframePointerStatus.update(i)
						return
					}
				}
			})
		} catch (e) {}
	}


	/**
	 * [incTimeEditPosition]
	 * increments the position of the time value editor
	 * 0 tenths of seconds
	 * 1 seconds
	 * 2 minutes
	 * @return none
	 */
	incTimeEditPosition() {
		this.timeEditPosition++;
		if (this.timeEditPosition > 2) {
			this.timeEditPosition = 2;
		}
		this.keyframeTimeEditStringStatus.update(this.getKeyframeTimeEditString())
	}

	/**
	 * [decTimeEditPosition]
	 * decrements the position of the time value editor
	 * 0 tenths of seconds
	 * 1 seconds
	 * 2 minutes
	 * @return none
	 */
	decTimeEditPosition() {
		this.timeEditPosition--;
		if (this.timeEditPosition < 0) {
			this.timeEditPosition = 0;
		}
		this.keyframeTimeEditStringStatus.update(this.getKeyframeTimeEditString())
	}

	/**
	 * [incTime]
	 * increments the time value at the current time edit position
	 * overflow is carried over to the next digit
	 * @return {[type]} [description]
	 */
	incTime() {
		switch (this.timeEditPosition) {
			case 0:
				this.incTenthSeconds();
				break;
			case 1:
				this.incSeconds();
				break;
			case 2:
				this.incMinutes();
				break;
			default:
				console.error("this.timeEditPosition");
				console.error(this.timeEditPosition);
				break;
		}
	}

	/**
	 * [decTime]
	 * decrements the time value at the current time edit position
	 * overflow is carried over to the next digit
	 * @return {[type]} [description]
	 */
	decTime() {
		switch (this.timeEditPosition) {
			case 0:
				this.decTenthSeconds();
				break;
			case 1:
				this.decSeconds();
				break;
			case 2:
				this.decMinutes();
				break;
			default:
				console.error("this.timeEditPosition");
				console.error(this.timeEditPosition);
				break;
		}
	}

	/**
	 * [incTenthSeconds]
	 * increments the tenths of seconds value of the editor
	 * overflow is carried over to the next digit
	 * @return {[type]} [description]
	 */
	incTenthSeconds() {
		this.timeValueSetting.update(this.timeValueSetting.value + 1);
	}

	/**
	 * [decTenthSeconds]
	 * decrements the tenths of seconds value of the editor
	 * overflow is carried over to the next digit
	 * @return {[type]} [description]
	 */
	decTenthSeconds() {
		this.timeValueSetting.update(this.timeValueSetting.value - 1);
	}

	/**
	 * [incSeconds]
	 * increments the seconds value of the editor
	 * overflow is carried over to the next digit
	 * @return {[type]} [description]
	 */
	incSeconds() {
		this.timeValueSetting.update(this.timeValueSetting.value + 10);
	}

	/**
	 * [decSeconds]
	 * decrements timeValue by 1 second (10 * 1/10s)
	 * @return {[type]} [description]
	 */
	decSeconds() {
		this.timeValueSetting.update(this.timeValueSetting.value - 10);
	}

	/**
	 * [incMinutes]
	 * increments the minutes value of the editor
	 * overflow is carried over to the next digit
	 * enshures maxMinutes limit is met
	 * @return {[type]} [description]
	 */
	incMinutes() {
		this.timeValueSetting.update(this.timeValueSetting.value + 600);
	}

	/**
	 * [decMinutes]
	 * decrements the minutes value of the editor
	 * overflow is carried over to the next digit
	 * @return {[type]} [description]
	 */
	decMinutes() {
		this.timeValueSetting.update(this.timeValueSetting.value - 600);
	}


	/**
	 * [getKeyframeTimeString]
	 * creates a string representation of the time editor in keyframeTimeString
	 * @return {[type]} [description]
	 */
	getKeyframeTimeString() {
		var time = this.getTime()
		return ("0" + time.minutes).slice(-2) + ":" + ("0" + time.seconds).slice(-2) + "." + time.tenthSeconds
	}

	/**
	 * [getKeyframeTimeEditString]
	 * creates a string representation of the time editor in keyframeTimeString
	 * @return {[type]} [description]
	 */
	getKeyframeTimeEditString() {
		var time = this.getTime()
		return ("0" + time.minutes).slice(-2) + this.timeEditPositionStrings[this.timeEditPosition] + ("0" + time.seconds).slice(-2) + "." + time.tenthSeconds
	}

	/**
	 * [compareKeyframes]
	 * compare function to use the sort function
	 * @param  {[type]} a [keyframe a]
	 * @param  {[type]} b [keyframe b]
	 * @return {[type]}   [0 equal, 1 a>b, -1 a<b]
	 */
	compareKeyframes(a, b) {
		if (a.t < b.t) {
			return -1
		}
		if (a.t > b.t) {
			return 1
		}
		return 0;
	}

	/**
	 * [sortKeyframes]
	 * sort the keyframes based on their time value
	 */
	sortKeyframes() {
		if (this.keyframes.length > 0) {
			this.keyframes.sort(this.compareKeyframes)
			console.info("sortKeyframes");
		}
	}

	/**
	 * [updateAllGcodes]
	 * updates the gcode of all keyframes because there may have been
	 * new keyframes been insertet between
	 */
	updateAllGcodes() {
		console.group("updateAllGcodes")
		this.keyframes[0].createGcode(0);
		for (var i = 1; i < this.keyframes.length; i++) {
			this.keyframes[i].createGcode(this.keyframes[i].t - this.keyframes[i - 1].t)
		}
		console.groupEnd()
	}

	/**
	 * [debug]
	 * print the keyframeEditor Object
	 */
	debug() {
		console.error(this);
	}
}

/**
 * keyframe class
 * calculates the F value upon creation
 * generates the gcode command to execute the keyframe
 */
class Keyframe {
	// constructor(x, y, z, a, t, includeA, includeB, includeX, includeY, f, gcode, keyframeIndex) {
	constructor(options) {
		this.interfaces = options.interfaces
		this.t = options.t
	}

	/**
	 * duration in 0.1s steps
	 */
	createGcode(duration) {
		console.group("createGcode", duration)
		if (duration == undefined) {
			console.error("duration must be defined");
			console.groupEnd()
			return;
		}
		for (const [name, interfaceVector] of Object.entries(this.interfaces)) {
			interfaceVector.code = keyframeEditor.interfaces[interfaceVector.name].createKeyframeCode(interfaceVector.axes, duration)
			console.log("[" + interfaceVector.name + "] " + interfaceVector.code);
		};
		console.groupEnd()
	}
}

var keyframeEditor = new KeyframeEditor()
module.exports = keyframeEditor;
