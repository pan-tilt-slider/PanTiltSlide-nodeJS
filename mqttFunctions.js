var console = require('./debug.js')
var mqtt = require("./mqttHandler.js").mqttHandler

const FUNCTIONS_BASETOPIC = require("./mqttBasetopics.js").FUNCTIONS_BASETOPIC

// calls all local callbacks when a value is written over mqtt.
// the written value is passed to all local callbacks and mirrored over mqtt
// once all callbacks have been executed.
// a local trigger of the function has the same effect

class FunctionStore {
	constructor() {
		console.log("setup FunctionStore");
		this._functions = []
	}
	// (name, callback, topic)
	addFunction(options) {
		console.group("addMqttFunction: " + options.name + ": " + options.topic);
		if (this._functions[options.name] == undefined) {
			this._functions[options.name] = new Function(options)
			console.groupEnd()
			return this._functions[options.name]
		} else {
			console.error("Function name is taken!");
			console.error(options.name);
			console.error(options.topic);
		}
		console.groupEnd()
	}

	addCallback(name, callback) {
		try {
			this._functions[name].addCallback(callback)
		} catch (e) {
			console.log(e);
		}
	}
}

class Function {
	// (name, callback, topic)
	constructor(options) {
		this.name = options.name
		this.callbacks = []

		if (options.topic != undefined) {
			this.basetopic = options.topic
		} else {
			this.basetopic = mqtt.BASETOPIC + "/" + FUNCTIONS_BASETOPIC + "/" + options.name
		}

		if (options.callback != undefined) {
			this.addCallback(options.callback)
		}

		mqtt.subscribe(this.basetopic + "/W", (value) => {
			this.callFunction(options.value)
		})
	}

	_executeCallbacks(value) {
		this.callbacks.forEach(callback => {
			callback(value)
		});
	}

	callFunction(value) {
		if (value == undefined) {
			value = ""
		}
		this._executeCallbacks(value);
		mqtt.publish(this.basetopic + "/R", value);
	}

	addCallback(callback) {
		this.callbacks.push(callback)
	}
}

var functionStore = new FunctionStore();
module.exports.functionStore = functionStore;
