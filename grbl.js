/**
 * $10=2
 */

var console = require('./debug.js')
var functionStore = require('./mqttFunctions.js').functionStore
var statusStore = require('./mqttStatus.js').statusStore
var mqtt = require("./mqttHandler.js").mqttHandler
const fs = require('fs');

const Interface = require("./interface.js")

const grblErrorCodes = JSON.parse(fs.readFileSync('grblErrorCodes.json'));
const grblAlarmCodes = JSON.parse(fs.readFileSync('grblAlarmCodes.json'));

// modes to control the flow of the grbl sender / receiver
const MODE_BASIC = 0
const MODE_CLEAR_ALL_BUFFERS = 1
const MODE_AWAIT_OK = 2
const MODE_PAUSE = 3

module.exports = class Grbl extends Interface {
	
	// constructor options
	// writeFunction, name, rxBufferMaxSpace, jogIncrementTime, statusInterval, sendbufferInterval, axes
	constructor(options) {
		console.group("new Grbl")
		console.log("setup interface");
		super({
			name: options.name,
			abbreviation: options.abbreviation,
			maxJogSpeed: options.maxJogSpeed,
			statusCallback: options.statusCallback,
			settingsCallback: options.settingsCallback,
			// functions required by the Interface parent class
			pause: () => {
				console.log("[" + this.name + "]" + "Pause");
				this.mode = MODE_PAUSE
			},
			play: () => {
				console.log("[" + this.name + "]" + "Play");
				this.mode = MODE_BASIC
			},
			addToCommandBuffer: (command) => {
				if (Array.isArray(command)) {
					this.commandBuffer = this.commandBuffer.concat(command)
				} else {
					this.commandBuffer.push(command)
				}
			},
			createKeyframeCode: (axes, duration) => {
				if (duration == undefined) {
					return;
				}
				var fNumber = 600 / duration;
				if (!isFinite(fNumber)) {
					fNumber = this.travelToFirstKeyframeSpeed;
				}
				var command = "G93G1"
				for (const [name, axis] of Object.entries(axes)) {
					command += axis.internalName + axis.value.toFixed(2)
				}
				command += "F"
				command += fNumber
				command += this.commandTerminator
				return command
			},
			moveSingleAxisFunction: (axis) => {
				console.log("[" + this.name + "]" + "move single axis: " + this.axes[axis.axisName].basetopic)
				console.log(axis);
				console.log("G0" + this.axes[axis.axisName].internalName + axis.value + this.commandTerminator)

				// TODO: does this do anything?
				this.createKeyframeCode(interfaceVector.axes, 0)

				this.addToCommandBuffer([
					'G94' + this.commandTerminator,
					'G90' + this.commandTerminator,
					"G0" + this.axes[axis.axisName].internalName + axis.value + this.commandTerminator
				])
			},
			moveToFunction: (target) => {
				console.log(this.name + " move to " + target);
				console.log(target);
				var buf = Buffer.from(target)
				console.log(buf.toString());
				var axisTargets = JSON.parse(buf.toString())

				var commands = [
					'G94' + this.commandTerminator,
					'G90' + this.commandTerminator
				]

				var targetCode = "G0"
				for (const [targetName, targetValue] of Object.entries(axisTargets)) {
					targetCode += this.axes[targetName].internalName + targetValue
				}
				targetCode += this.commandTerminator

				commands.push(targetCode)

				this.addToCommandBuffer(commands)

			}
		})

		var self = this;
		this.writeFunction = options.writeFunction

		this.commandTerminator = "\r"

		// buffer for answers from grbl
		this.grblReceiveBuffer = ""
		this.debugInterval = options.debugInterval
		this.debugTimestamp = 0;

		// modes and callbacks to control flow for more restricted operations
		// e.g. homing
		// the callbacks bear the responsibility to clear themself
		// and eventually return to MODE_BASIC
		this.mode = MODE_BASIC;
		this.clearAllBuffersCallback = undefined;
		this.awaitOkCallback = undefined;

		// the interval with which to call the status function of grbl
		// the status call updates the local coordinates
		this.statusInterval = options.statusInterval;

		this.sendbufferInterval = options.sendbufferInterval;

		this.travelToFirstKeyframeSpeed = options.travelToFirstKeyframeSpeed

		// reference to the status update interval
		// will be fille once the seriall connection is established
		this.statusIntervalTimer = undefined;
		this.sendBufferIntervalTimer = undefined;
		// not used yet
		// will be used to count sent and parsed bytes to maximize throuput
		// to grbl
		this.rxBufferMaxSpace = options.rxBufferMaxSpace;
		// buffer array for gcode commands
		// the content of this buffer will be sent automatically to grbl
		this.commandBuffer = [];

		// time for a single jog command
		// used to calculate jog steps
		// shorter times for shorter delay => requires more responsive connection
		this.jogIncrementTime = options.jogIncrementTime;

		// set true to cancel all remaining jog commands
		this._jogCancel = false;

		// function to call after the position of grbl has been updated
		this.positionUpdateCallback = undefined;

		// the number of bytes in the grbl rx buffer
		this.bytesInRxBuffer = 0;

		// mimics the messages in the rx buffer
		this.sentBytes = [];

		console.log("setup Intervals");

		self.statusIntervalTimer = setInterval(() => {
			if (this.mode == MODE_BASIC) {
				self.statusQuery();
			}
		}, self.statusInterval)

		self.sendBufferIntervalTimer = setInterval(() => {
			if (this.mode == MODE_CLEAR_ALL_BUFFERS) {
				if (this.bytesInRxBuffer == 0) {
					try {
						this.clearAllBuffersCallback()
					} catch (e) {
						console.error("[" + this.name + "]" + "clearAllBuffersCallback not defined");
						console.error("[" + this.name + "]" + e);
					}
				}
			}
			if (this.mode == MODE_BASIC) {
				if (this.commandBuffer.length > 0) {
					if (this._write(this.commandBuffer[0])) {
						// return true if send was successful
						this.commandBuffer.shift()
					}
				} else {
					// jog cancel
					if (this._jogCancel) {
						for (const [axisName, axis] of Object.entries(this.axes)) {
							axis.setJogFactor(0)
						}

						// only cancel jog if all bytes are out of the receive buffer of grbl
						if (this.bytesInRxBuffer == 0) {
							this._jogCancel = false;

							// send jog cancel
							console.log("[" + this.name + "]" + "jog cancel");
							this.writeFunction(Buffer.from([133]));
						}
					} else {
						// if (this.jogX != 0 || this.jogY != 0 || this.jogZ != 0 || this.jogA != 0) {
						if (Object.entries(this.axes).some(([name, axis]) => axis.getJogFactor() != 0)) {
							// only jog when no unparsed commands are in buffer
							// otherwise grbl will move after jog cancel
							if (this.bytesInRxBuffer == 0) {
								this._jogSpeed();
							}
						}
					}
				}
			}
		}, self.sendBufferInterval)


		console.log("setup axis");
		// axis can only be added once a mqtt connection is established
		// because axis uses mqttSettings etc
		options.axes.forEach((axis) => {
			this.addAxis(axis)
		});

		this.positionUpdateCallback = options.positionUpdateCallback

		console.log("setup errors");
		console.log("setup alarms");
		this.status.addCallback((value) => {
			if (value == "Idle") {
				this.grblError.update(grblErrorCodes[0])
				this.grblAlarm.update(grblAlarmCodes[0])
				if (this.bytesInRxBuffer != 0) {
					console.error("[" + this.name + "] BUFFER LEAK");
				}
			}
		})

		this.grblErrorMessage = statusStore.addStatus({
			name: this.basetopic + "/error/message",
			value: grblErrorCodes[0].message,
			topic: this.basetopic + "/error/message"
		})

		this.grblErrorDescription = statusStore.addStatus({
			name: this.basetopic + "/error/description",
			value: grblErrorCodes[0].description,
			topic: this.basetopic + "/error/description"
		})

		this.grblError = statusStore.addStatus({
			name: this.basetopic + "/error",
			value: grblErrorCodes[0],
			callback: (newError) => {
				this.grblErrorMessage.update(newError.message)
				this.grblErrorDescription.update(newError.description)
			},
			topic: this.basetopic + "/error"
		})

		this.grblAlarmMessage = statusStore.addStatus({
			name: this.basetopic + "/alarm/message",
			value: grblAlarmCodes[0].message,
			topic: this.basetopic + "/alarm/message"
		})

		this.grblAlarmDescription = statusStore.addStatus({
			name: this.basetopic + "/alarm/description",
			value: grblAlarmCodes[0].description,
			topic: this.basetopic + "/alarm/description"
		})

		this.grblAlarm = statusStore.addStatus({
			name: this.basetopic + "/alarm",
			value: grblAlarmCodes[0],
			callback: (newAlarm) => {
				this.grblAlarmMessage.update(newAlarm.message)
				this.grblAlarmDescription.update(newAlarm.description)
			},
			topic: this.basetopic + "/alarm"
		})

		console.log("setup functions");
		functionStore.addFunction({
			name: this.basetopic + "/killAlarmLock",
			callback: () => {
				self.killAlarmLock();
			},
			topic: this.basetopic + "/killAlarmLock"
		})

		functionStore.addFunction({
			name: this.basetopic + "/softReset",
			callback: () => {
				self.softReset();
			},
			topic: this.basetopic + "/softReset"
		})

		functionStore.addFunction({
			name: this.basetopic + "/resume",
			callback: () => {
				self.resume()
			},
			topic: this.basetopic + "/resume"
		})

		functionStore.addFunction({
			name: this.basetopic + "/statusReportQuery",
			callback: () => {
				self.statusQuery();
			},
			topic: this.basetopic + "/statusReportQuery"
		})

		functionStore.addFunction({
			name: this.basetopic + "/feedHold",
			callback: () => {
				self.feedHold()
			},
			topic: this.basetopic + "/feedHold"
		})

		functionStore.addFunction({
			name: this.basetopic + "/jogCancel",
			callback: () => {
				self.jogCancel();
			},
			topic: this.basetopic + "/jogCancel"
		})

		functionStore.addFunction({
			name: this.basetopic + "/home",
			callback: () => {
				self.home();
			},
			topic: this.basetopic + "/home"
		})

		console.groupEnd()
	}

	read(line) {
		this.grblReceiveBuffer = line
		this.grblReceiveBuffer = this.grblReceiveBuffer.replace("\n", "").replace("\r", "");
		var responseHasBeenIdentified = false;

		// debug messages
		// define debugInterval as constructor option to activate
		// value is interval in ms
		if (this.debugInterval != undefined) {
			if (Date.now() - this.debugTimestamp > this.debugInterval) {
				this.debugTimestamp = Date.now()
				console.group("[" + this.name + "] grbl interface debug")
				console.info("[" + this.name + "] line: " + line)
				console.info("[" + this.name + "] grblReceiveBuffer: " + this.grblReceiveBuffer)
				console.info("[" + this.name + "] bytesInRxBuffer: " + this.bytesInRxBuffer)
				console.info("[" + this.name + "] rxBufferMaxSpace: " + this.rxBufferMaxSpace)
				console.info("[" + this.name + "] status: " + this.status.value)
				console.info("[" + this.name + "] _jogCancel: " + this._jogCancel)
				console.groupEnd()
			}
		}

		// skip empty
		if (this.grblReceiveBuffer != "") {

			// command has been proccessed => remove from sendbuffer
			if (this.grblReceiveBuffer == "ok" || this.grblReceiveBuffer.startsWith("error:")) {
				responseHasBeenIdentified = true

				// translate error code to text
				if (this.grblReceiveBuffer.startsWith("error:")) {
					this.grblError.update(grblErrorCodes[parseInt(this.grblReceiveBuffer.replace("error:", ""))]);
				}

				// remove first entry from  sentByte FIFO List because it has been processed
				var first = this.sentBytes.shift()
				if (first != undefined) {
					this.bytesInRxBuffer -= first;
				} else {
					console.error("[" + this.name + "] SENT BYTES FIRST ENTRY UNDEFINED");
				}

				if (this.mode == MODE_AWAIT_OK) {
					if (this.grblReceiveBuffer == "ok") {
						try {
							this.awaitOkCallback()
						} catch (e) {
							console.error("[" + this.name + "]" + "awaitOkCallback not defined");
							console.error("[" + this.name + "]" + e);
						}
					}
				}
			} else if (this.grblReceiveBuffer.startsWith("ALARM:")) {
				responseHasBeenIdentified = true
				// update alarm state
				this.grblAlarm.update(grblAlarmCodes[parseInt(this.grblReceiveBuffer.replace("ALARM:", ""))])
			}

			// parse statusQuery response
			if (this.grblReceiveBuffer.startsWith("<")) {
				responseHasBeenIdentified = true
				var fields = this.grblReceiveBuffer.split("|")

				for (const field in fields) {
					if (fields.hasOwnProperty(field)) {
						const element = fields[field];

						if (element.startsWith("<")) {
							this.status.update(element.replace("<", ""));
						}

						if (element.startsWith("WPos") || element.startsWith("MPos")) {
							if (element.indexOf(",") < 0) {
								continue;
							}

							var dirty = false;

							var coordinates = element.replace("WPos:", "").replace("MPos:", "").split(",");

							if (coordinates == undefined) {
								continue;
							}
							Object.entries(this.axes).forEach(([axisName, axis], i) => {
								try {
									var value = parseFloat(coordinates[i]);
									if (value !== NaN) {
										if (value != axis.getValue()) {
											dirty = true;
											axis.setValue(value)
										}
									} else {
										console.error("parseFloat messed up");
										console.error(coordinates[i]);
									}
								} catch (e) {
									console.error("Not enoug axes in grbl firmware defined");
									console.error(e);
								}
							});

							// update if any value has changed
							if (dirty) {
								if (this.positionUpdateCallback != undefined) {
									this.positionUpdateCallback();
								}
							}
						}
					}
				}
			}
			if (!responseHasBeenIdentified) {
				console.error("[" + this.name + "] Unidentified response");
				console.error("[" + this.name + "] line: " + line)
				console.error("[" + this.name + "] grblReceiveBuffer: " + this.grblReceiveBuffer)
				console.error("[" + this.name + "] bytesInRxBuffer: " + this.bytesInRxBuffer)
				console.error("[" + this.name + "] rxBufferMaxSpace: " + this.rxBufferMaxSpace)
				console.error("[" + this.name + "] status: " + this.status.value)
			}
		}
		this.grblReceiveBuffer = ""
	}


	_write(command) {
		if (this.bytesInRxBuffer + command.length <= this.rxBufferMaxSpace) {
			this.bytesInRxBuffer += command.length;
			this.sentBytes.push(command.length)
			this.writeFunction(Buffer.from(command, 'utf-8'));
			return true;
		} else {
			// console.error("[" + this.name + "]" + "CANT SEND");
			// console.error("[" + this.name + "]" + "BUFFER WOULD OVERFLOW");
			// console.error("[" + this.name + "]" + "command: " + command);
			// console.error("[" + this.name + "]" + "command.length: " + command.length);
			// console.error("[" + this.name + "]" + "this.rxBufferMaxSpace: " + this.rxBufferMaxSpace);
			// console.error("[" + this.name + "]" + "this.bytesInRxBuffer: " + this.bytesInRxBuffer);
			return false;
		}
	}

	// realtime commands
	killAlarmLock() {
		this.writeFunction(Buffer.from("$X" + this.commandTerminator))
		this.bytesInRxBuffer = 0;
		this.sentBytes = [];
	}
	resume() {
		this.writeFunction(Buffer.from('~'))
	}
	feedHold() {
		this.writeFunction(Buffer.from('!'))
	}
	statusQuery() {
		this.writeFunction(Buffer.from("?"))
	}
	softReset() {
		this.writeFunction(Buffer.from([24]));
		this.bytesInRxBuffer = 0;
		this.sentBytes = [];
	}

	// sets the jog factors
	// the sendBufferInterval handles the rest
	jog(factors) {
		// limit imput to minimum if >0
		for (const [key, value] of Object.entries(factors)) {
			var newValue = value
			try {
				var axis = this.axes[key]
				// implement deadzone
				if ((Math.abs(newValue) < axis.jogMin) && (newValue != 0)) {
					if (newValue < 0) {
						axis.setJogFactor(axis.jogMin * -1)
					} else {
						axis.setJogFactor(axis.jogMin)
					}
				}
				axis.setJogFactor(newValue)
			} catch (e) {
				console.error("Axis not defined");
				console.error(e);
			}
		}

		if (Object.entries(this.axes).every(([axisName, axis]) => {
				axis.getJogFactor() == 0
			})) {
			this.jogCancel();
		}
	}

	/**
	 * call repeatedly for joystic jogging
	 */
	_jogSpeed() {

		var jogSpeed = 0

		Object.entries(this.axes).forEach(([axisName, axis], i) => {
			jogSpeed += Math.pow(axis.getJogFactor(), 2)
		});

		jogSpeed = Math.abs(Math.round(this.maxJogSpeed.value * (Math.sqrt(jogSpeed))))

		// temp value to calculate speed vectors
		var q = (this.maxJogSpeed.value / 60) * this.jogIncrementTime

		// header for all jog commands
		var command = '$J=G91'

		// dirty flag to move only when there is movement
		var anyAxisIsNotZero = true
		Object.entries(this.axes).forEach(([axisName, axis], i) => {
			var s = axis.getJogFactor() * q
			if (s != 0) {
				anyAxisIsNotZero = false;
				command += axis.internalName + s.toFixed(2)
			}
		})

		command += "F"
		command += jogSpeed
		command += this.commandTerminator

		// skip if all are 0
		if (anyAxisIsNotZero) {
			return;
		}
		this.addToCommandBuffer(command);
	}

	jogCancel() {
		//real time command => no response
		this._jogCancel = true;
	}

	setZero() {
		console.log("[" + this.name + "]" + "set zero");
		var command = "G10P1L20"
		Object.entries(this.axes).forEach(([axisName, axis], i) => {
			command += axis.internalName + "0"
		});
		command += this.commandTerminator
		console.log(command);
		this.addToCommandBuffer(command);
	}

	// homing blocks all other interaction with grbl
	home() {
		console.log("[" + this.name + "]" + "home");
		this.status.update("Home")

		// wait until all buffers are clear => all actions have been performed
		this.mode = MODE_CLEAR_ALL_BUFFERS
		this.clearAllBuffersCallback = () => {
			console.log("[" + this.name + "]" + "clearAllBuffersCallback");
			this.clearAllBuffersCallback = undefined

			// send the homing command and wait for the "ok" after it finishes
			this.writeFunction(Buffer.from("$h" + this.commandTerminator))
			this.mode = MODE_AWAIT_OK
			this.awaitOkCallback = () => {
				// when the "ok" has been received homing was a success
				// now we can add any offsets and zero the workspace
				console.log("[" + this.name + "]" + "awaitOkCallback");
				this.mode = MODE_BASIC
				this.commandBuffer = [];
				// TODO 
				// Make this a option for the grbl instance.
				// => Not every instance has the same offset
				this.addToCommandBuffer("G91G0Y-3.5X-1.5" + this.commandTerminator)
				this.setZero()
			}
		}
	}

	writeGrbl(command) {
		this.addToCommandBuffer(command)
	}
}
