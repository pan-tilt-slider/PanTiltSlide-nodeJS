var console = require('./debug.js')
var mqtt = require("./mqttHandler.js").mqttHandler
const SETTINGS_BASETOPIC = require("./mqttBasetopics.js").SETTINGS_BASETOPIC

// Executes the callback when a new value
// is assigned over MQTT. the received value
// is passed to every local callback
// If the setting is locally updated the new value
// will also be passed to all callbacks and published
// over mqtt.


// TODO: make settings persistent
// 			- JSON settings files
// 			- if new setting is created, look if it already exists and load
// 			  the previous value

class SettingStore {
	constructor() {
		this._settings = [];
	}

	// (name, value, callback, topic)
	addSetting(options) {
		console.group("addMqttSetting: " + options.name + ": " + options.topic);
		if (this._settings[options.name] == undefined) {
			this._settings[options.name] = new Setting(options)
			console.groupEnd()
			return this._settings[options.name]
		} else {
			console.error("Setting name already taken: " + options.name);
		}
		console.groupEnd()
	}

	addCallback(name, callback) {
		this._settings[name].callbacks.push(callback)
	}

	update(name, value) {
		try {
			this._settings[name].update(value)
		} catch (e) {
			console.error(e);
		}
	}
}

class Setting {
	// (name, value, callback, topic)
	constructor(options) {
		this.name = options.name
		this.value = undefined
		this.callbacks = []
		this.setter = options.setter

		if (options.topic != undefined) {
			this.basetopic = options.topic
		} else {
			this.basetopic = mqtt.BASETOPIC + "/" + SETTINGS_BASETOPIC + "/" + options.name
		}

		if (options.callback != undefined) {
			this.addCallback(options.callback)
		}
		mqtt.subscribe(this.basetopic + "/W", (value) => {
			this._setValue(value)
		})
		if (options.value != undefined) {
			this.update(options.value, true)
		}
	}
	_setValue(value) {
		if (this.setter != undefined) {
			console.log("[" + this.name + "]" + "Invoking setter");
			value = this.setter(value)
			console.log("[" + this.name + "]" + "Result: " + value);
		}
		console.log("[" + this.name + "]" + "Set setting: " + JSON.stringify(value));
		this.value = value;
		this._executeCallbacks();
		mqtt.publish(this.basetopic + "/R", this.value);
	}

	_executeCallbacks() {
		this.callbacks.forEach(callback => {
			callback(this.value)
		});
	}

	update(value, force) {
		if ((value !== this.value) || force) {
			this._setValue(value)
		}
	}

	addCallback(callback) {
		this.callbacks.push(callback)
	}
}

var settingStore = new SettingStore();
module.exports.settingStore = settingStore;
